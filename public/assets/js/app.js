$( document ).ready(function() {

	$('input, textarea').on('focus focusout', function(event) {

		$e = $(this);

		if (event.type == 'focus') {
			$placeholder = $e.attr("placeholder");
			$e.attr("ph-input", $placeholder);
			$placeholder = '';
		}
		if (event.type == 'focusout') {
			$placeholder = $e.attr("ph-input");
		}
		$e.attr("placeholder", $placeholder);
	});

	$('.ajaxForm').on('submit', function(event) {
		event.preventDefault();
		callAjax($(this));
	});

});



	function callAjax($e, $o) {
		
		$.ajax({
			method: 'POST',
			url: $e.attr('action'),
			data: $e.serialize(),
			complete: function(xhr, textStatus) {
				  //$.fancybox.open(xhr.responseText);
			},
			error: function(xhr, status, error) {


				var errors = '';
				$.each(xhr.responseJSON.errors, function() {
					errors += "<div>" + this[0] + "</div>";
				});

				$modal = '<div style="cursor:default; margin-top: -100px">'+
					'<h3 style="padding: 20px 0 0 0">' + xhr.responseJSON.messages + '</h3><br>'+
					errors
				'</div>';

				$.fancybox.open($modal + "<button style='margin: 10px 0 0 40%' class='btn_submit bg_cs' onclick='javascript:parent.jQuery.fancybox.close();' href='#'>OK</button>");

				console.log(xhr.responseJSON);
			},
			success: function(r) {

				if ($o.onSuccess != undefined) {
					$o.onSuccess(r);
				}

				//console.log($e[0].reset());
				//$modal2 = '<div style="cursor:default; margin-top: -100px">'+
					'<h3 style="padding: 20px 0 0 0">' + r.message + '!</h3>';

				//$.fancybox.open($modal2 + "<button style='margin: 10px 0 0 35%' class='btn_submit bg_cs' onclick='javascript:parent.jQuery.fancybox.close();' href='#'>OK</button></div>");
			

			}
		});
	}