(function ($) {
jQuery(document).on('ready', function () {
if (document.getElementById("particles-js")) particlesJS("particles-js", {
"particles": {
"number": {
"value": 50,
"density": {
"enable": !0,
"value_area": 600
}
},
"color": {
"value": ["#28085a", "#21064c", "#270857"]
},
"shape": {
"type": "circle",
"stroke": {
"width": 0,
"color": "#fff"
},
"polygon": {
"nb_sides": 5
},
"image": {
"src": "img/github.svg",
"width": 350,
"height": 350
}
},
"opacity": {
"value": 1,
"random": !0,
"anim": {
"enable": !0,
"speed": 0.2,
"opacity_min": 0,
"sync": !1
}
},
"size": {
"value": 130,
"random": !0,
"anim": {
"enable": !0,
"speed": 2,
"size_min": 5,
"sync": !1
}
},
"line_linked": {
"enable": !1,
"distance": 150,
"color": "#ffffff",
"opacity": 0.4,
"width": 1
},
"move": {
"enable": !0,
"speed": 1,
"direction": "top",
"random": !0,
"straight": !1,
"out_mode": "out",
"bounce": !1,
"attract": {
"enable": !1,
"rotateX": 600,
"rotateY": 600
}
}
},
"interactivity": {
"detect_on": "canvas",
"events": {
"onhover": {
"enable": !1,
"mode": "bubble"
},
"onclick": {
"enable": !1,
"mode": "repulse"
},
"resize": !0
},
"modes": {
"grab": {
"distance": 400,
"line_linked": {
"opacity": 1
}
},
"bubble": {
"distance": 250,
"size": 0,
"duration": 2,
"opacity": 0,
"speed": 3
},
"repulse": {
"distance": 400,
"duration": 0.4
},
"push": {
"particles_nb": 4
},
"remove": {
"particles_nb": 2
}
}
},
"retina_detect": !0
});
});
}(jQuery));

(function ($) {
jQuery(document).on('ready', function () {
if (document.getElementById("particles-js_a")) particlesJS("particles-js_a", {
"particles": {
"number": {
"value": 50,
"density": {
"enable": !0,
"value_area": 600
}
},
"color": {
"value": ["#28085a", "#21064c", "#270857"]
},
"shape": {
"type": "circle",
"stroke": {
"width": 0,
"color": "#fff"
},
"polygon": {
"nb_sides": 5
},
"image": {
"src": "img/github.svg",
"width": 350,
"height": 350
}
},
"opacity": {
"value": 1,
"random": !0,
"anim": {
"enable": !0,
"speed": 0.2,
"opacity_min": 0,
"sync": !1
}
},
"size": {
"value": 130,
"random": !0,
"anim": {
"enable": !0,
"speed": 2,
"size_min": 5,
"sync": !1
}
},
"line_linked": {
"enable": !1,
"distance": 150,
"color": "#ffffff",
"opacity": 0.4,
"width": 1
},
"move": {
"enable": !0,
"speed": 1,
"direction": "top",
"random": !0,
"straight": !1,
"out_mode": "out",
"bounce": !1,
"attract": {
"enable": !1,
"rotateX": 600,
"rotateY": 600
}
}
},
"interactivity": {
"detect_on": "canvas",
"events": {
"onhover": {
"enable": !1,
"mode": "bubble"
},
"onclick": {
"enable": !1,
"mode": "repulse"
},
"resize": !0
},
"modes": {
"grab": {
"distance": 400,
"line_linked": {
"opacity": 1
}
},
"bubble": {
"distance": 250,
"size": 0,
"duration": 2,
"opacity": 0,
"speed": 3
},
"repulse": {
"distance": 400,
"duration": 0.4
},
"push": {
"particles_nb": 4
},
"remove": {
"particles_nb": 2
}
}
},
"retina_detect": !0
});
});
}(jQuery));



(function ($) {
jQuery(document).on('ready', function () {
if (document.getElementById("particles-js_b")) particlesJS("particles-js_b", {
"particles": {
"number": {
"value": 50,
"density": {
"enable": !0,
"value_area": 600
}
},
"color": {
"value": ["#28085a", "#21064c", "#270857"]
},
"shape": {
"type": "circle",
"stroke": {
"width": 0,
"color": "#fff"
},
"polygon": {
"nb_sides": 5
},
"image": {
"src": "img/github.svg",
"width": 350,
"height": 350
}
},
"opacity": {
"value": 1,
"random": !0,
"anim": {
"enable": !0,
"speed": 0.2,
"opacity_min": 0,
"sync": !1
}
},
"size": {
"value": 130,
"random": !0,
"anim": {
"enable": !0,
"speed": 2,
"size_min": 5,
"sync": !1
}
},
"line_linked": {
"enable": !1,
"distance": 150,
"color": "#ffffff",
"opacity": 0.4,
"width": 1
},
"move": {
"enable": !0,
"speed": 1,
"direction": "top",
"random": !0,
"straight": !1,
"out_mode": "out",
"bounce": !1,
"attract": {
"enable": !1,
"rotateX": 600,
"rotateY": 600
}
}
},
"interactivity": {
"detect_on": "canvas",
"events": {
"onhover": {
"enable": !1,
"mode": "bubble"
},
"onclick": {
"enable": !1,
"mode": "repulse"
},
"resize": !0
},
"modes": {
"grab": {
"distance": 400,
"line_linked": {
"opacity": 1
}
},
"bubble": {
"distance": 250,
"size": 0,
"duration": 2,
"opacity": 0,
"speed": 3
},
"repulse": {
"distance": 400,
"duration": 0.4
},
"push": {
"particles_nb": 4
},
"remove": {
"particles_nb": 2
}
}
},
"retina_detect": !0
});
});
}(jQuery));

$(function () {
$('.slick_x').slick({
dots: false,
infinite: true,
speed: 300,
slidesToShow: 2,
slidesToScroll: 1,
responsive: [{
breakpoint: 1024,
settings: {
slidesToShow: 1,
slidesToScroll: 1,
infinite: true,
dots: false
}
},
{
breakpoint: 600,
settings: {
slidesToShow: 1,
slidesToScroll: 1
}
},
{
breakpoint: 480,
settings: {
slidesToShow: 1,
slidesToScroll: 1
}
}
]
});

$('.slick_rew').slick({
centerMode: true,
dots: true,
centerPadding: '60px',
slidesToShow: 3,
responsive: [
{
breakpoint: 768,
settings: {
arrows: false,
centerMode: true,
centerPadding: '40px',
slidesToShow: 1
}
},
{
breakpoint: 480,
settings: {
arrows: false,
centerMode: true,
centerPadding: '20px',
slidesToShow: 1
}
}
]
});

$(".w_video").click(function () {
$(".drop_image_c").toggleClass("active");
});

});

$('.toggle_password').click(function(){
$(this).children().toggleClass('show_pass');
let input = $(this).prev();
input.attr('type', input.attr('type') === 'password' ? 'text' : 'password');
});

$(function() {
$(".spoiler.is-active").children(".spoiler-content").slideDown(); 
$(".spoiler-title").click(function() {
$(this).parent().siblings(".spoiler").removeClass("is-active").children(".spoiler-content").slideUp();
$(this).parent().toggleClass("is-active").children(".spoiler-content").slideToggle("ease-out");
});
});


$(document).ready(function () {
jQuery('.tab .tabs li a').on('click', function(e)  {
var currentAttrValue = jQuery(this).attr('href');
jQuery('.tab_content .tabs_item' + currentAttrValue).fadeIn(400).siblings().hide(); 
jQuery(this).parent('.tabs li').addClass('current').siblings().removeClass('current');
e.preventDefault();
});
});

$(".open_menu, .sidebar-overlay").click(function () {
$(".sidebar-overlay").toggleClass("active");
$(".sidebar").toggleClass("active");
});

$(".sidebar .close").click(function () {
$(".sidebar-overlay").toggleClass("active");
$(".sidebar").toggleClass("active");
});

$('.count').each(function () {
$(this).prop('Counter',0).animate({
Counter: $(this).text()
}, {
duration: 3000,
easing: 'swing',
step: function (now) {
$(this).text(Math.ceil(now));
}
});


});



$( "ul" ).on( "click", "li", function() {
var pos = $(this).index()+2;
$("tr").find('td:not(:eq(0))').hide();
$('td:nth-child('+pos+')').css('display','table-cell');
$("tr").find('th:not(:eq(0))').hide();
$('li').removeClass('active_a');
$(this).addClass('active_a');
});


var mediaQuery = window.matchMedia('(min-width: 940px)');
mediaQuery.addListener(doSomething);
function doSomething(mediaQuery) {    
if (mediaQuery.matches) {
$('.sep').attr('colspan',4);
} else {
$('.sep').attr('colspan',2);
}
}


doSomething(mediaQuery);

function handleFileSelect(evt) {
var files = evt.target.files; 
for (var i = 0, f; f = files[i]; i++) {
if (!f.type.match('image.*')) {
continue;
}
var reader = new FileReader();
reader.onload = (function(theFile) {
return function(e) {
/*var span = document.createElement('span');
span.innerHTML = ['<img class="thumb" src="', e.target.result,
            '" title="', escape(theFile.name), '"/>'].join('');
document.getElementById('list').insertBefore(span, null);*/
$('.company--image img').attr('src',e.target.result);
};
})(f);
reader.readAsDataURL(f);
}
}
$('#files').change(handleFileSelect);



/* Show More */
$(document).ready(function(){
$(".no_disp_offer").slice(0,4).css('display', 'inline-block');
$(".btn_more").click(function(e){
e.preventDefault();
$(".no_disp_offer:hidden").slice(0,4).fadeIn("slow").css('display', 'inline-block');
if($(".no_disp_offer:hidden").length == 0){
$(".btn_more").fadeOut("slow");
}
});
})
/* Show More */



$(document).ready(function () {
$('.modal_info').on('click', function () {
let divID = $(this).attr('data-box');
let id = $(this).attr('data-id');
let price = $(this).attr('data-price');
$('#check--from').find('input[name=id]').val(id);
$('#check--from').find('input[name=price]').val(price);
$('.check--sum').text(price);
    $('.tarification_checkout .info:not(.hidden)').addClass('hidden');
$('.tarification_checkout .info[data-id='+id+']').removeClass('hidden');
$(this).addClass('modal_open').siblings().removeClass('modal_open');
$('#' + divID).addClass('modal_open').siblings().removeClass('modal_open');
$('.modal_overlay').fadeIn(400);
});
$('.drop_info_close, .modal_overlay').on('click', function () {
$('.drop_info_up').removeClass('modal_open');
$('.modal_overlay').fadeOut(400);
});
});