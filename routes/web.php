<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/admin/login', 'Admin\LoginController@index')->name('admin.login');
Route::post('/admin/login', 'Admin\LoginController@login');
Route::get('/admin/logout', 'Admin\LoginController@logout')->name('admin.logout');

Route::group(['namespace' => 'Admin','prefix' => 'admin','middleware' => ['admin']], function () {
    Route::get('/', 'DashboardController@index')->name('admin.index');

    Route::get('/pages', 'ContentController@index')->name('pages.index');
    Route::get('/pages/show', 'ContentController@show');
    Route::post('/pages/info', 'ContentController@info');
    Route::get('/pages/edit/{id}', 'ContentController@edit');
    Route::post('/pages/edit/{id}', 'ContentController@update');
    Route::post('/pages/search', 'ContentController@search');

    Route::get('/reviews', 'ReviewController@index')->name('reviews.index');
    Route::get('/reviews/show', 'ReviewController@show');
    Route::post('/reviews/update', 'ReviewController@update');
    Route::post('/reviews/delete', 'ReviewController@delete');
    Route::post('/reviews/published', 'ReviewController@published');

    Route::get('/promocodes', 'PromocodeController@index')->name('admin.promocodes.index');
    Route::get('/promocodes/show', 'PromocodeController@show');
    Route::post('/promocodes/update', 'PromocodeController@update');
    Route::post('/promocodes/store', 'PromocodeController@store');
    Route::post('/promocodes/delete', 'PromocodeController@delete');

    Route::get('/domains', 'DomainController@index')->name('admin.domains.index');
    Route::get('/domains/change/{id}', 'DomainController@change')->name('admin.domain.change');
    Route::get('/domains/show', 'DomainController@show');
    Route::post('/domains/update', 'DomainController@update');
    Route::post('/domains/store', 'DomainController@store');
    Route::post('/domains/delete', 'DomainController@delete');
    Route::post('/domains/published', 'DomainController@published');

    Route::get('/faqs', 'FaqController@index')->name('admin.faqs.index');
    Route::get('/faqs/show', 'FaqController@show');
    Route::post('/faqs/store', 'FaqController@store');
    Route::post('/faqs/update', 'FaqController@update');
    Route::post('/faqs/delete', 'FaqController@delete');
    Route::post('/faqs/published', 'FaqController@published');

    Route::get('/services', 'ServiceController@index')->name('admin.services.index');
    Route::get('/services/show', 'ServiceController@show');
    Route::post('/services/store', 'ServiceController@store');
    Route::post('/services/update', 'ServiceController@update');
    Route::post('/services/delete', 'ServiceController@delete');
    Route::get('/services/stats/show/{id}', 'ServiceStatsController@index');
    Route::get('/services/stats/show', 'ServiceStatsController@show');
    Route::post('/services/stats/store', 'ServiceStatsController@store');
    Route::post('/services/stats/published', 'ServiceStatsController@published');
    Route::post('/services/stats/update', 'ServiceStatsController@update');
    Route::post('/services/stats/delete', 'ServiceStatsController@delete');

    Route::get('/reports', 'ReportController@index')->name('admin.reports.index');
    Route::get('/reports/show', 'ReportController@show');
    Route::post('/reports/store', 'ReportController@store');
    Route::post('/reports/update', 'ReportController@update');
    Route::post('/reports/delete', 'ReportController@delete');
    Route::get('/reports/galleries/show/{id}', 'ReportGalleriesController@index');
    Route::get('/reports/galleries/show', 'ReportGalleriesController@show');
    Route::post('/reports/galleries/store', 'ReportGalleriesController@store');
    Route::post('/reports/galleries/published', 'ReportGalleriesController@published');
    Route::post('/reports/galleries/update', 'ReportGalleriesController@update');
    Route::post('/reports/galleries/delete', 'ReportGalleriesController@delete');

    Route::get('/slider', 'SliderController@index')->name('slider.index');
    Route::get('/slider/show', 'SliderController@show');
    Route::post('/slider/edit', 'SliderController@edit');
    Route::post('/slider/update', 'SliderController@update');
    Route::post('/slider/store', 'SliderController@store');
    Route::post('/slider/delete', 'SliderController@delete');
    Route::post('/slider/published', 'SliderController@published');

    Route::get('/companies', 'CompanyController@index')->name('admin.companies.index');
    Route::get('/companies/show', 'CompanyController@show');
    Route::get('/companies/filter', 'CompanyController@filter');
    Route::post('/companies/update', 'CompanyController@update');
    Route::post('/companies/store', 'CompanyController@store');
    Route::post('/companies/delete', 'CompanyController@delete');

    Route::get('/users', 'UsersController@index')->name('admin.users.index');
    Route::get('/users/show', 'UsersController@show');
    Route::get('/users/payments/{id}', 'UsersController@payments');
    Route::post('/users/payments/show', 'UsersController@userPayments');
    Route::post('/users/update', 'UsersController@update');
    Route::post('/users/store', 'UsersController@store');
    Route::post('/users/delete', 'UsersController@delete');

    Route::get('/payments', 'PaymentsController@index')->name('admin.payments.index');
    Route::get('/payments/show', 'PaymentsController@show');
    Route::post('/payments/delete', 'PaymentsController@delete');
    Route::post('/payments/archive', 'PaymentsController@archive');
    Route::post('/payments/reestablish', 'PaymentsController@reestablish');
    Route::get('/payments/archive', 'PaymentsController@getArchive');
    Route::get('/payments/filter', 'PaymentsController@filter');
    Route::post('/payments/refund', 'PaymentsController@refund');

    Route::get('/contacts', 'ContactController@index')->name('contacts.index');
    Route::get('/contacts/show', 'ContactController@show');
    Route::post('/contacts/update', 'ContactController@update');

    Route::get('/countries', 'CountryController@index')->name('admin.countries.index');
    Route::get('/countries/show', 'CountryController@show');
    Route::post('/countries/update', 'CountryController@update');
    Route::post('/countries/store', 'CountryController@store');
    Route::post('/countries/delete', 'CountryController@delete');
    Route::post('/countries/published', 'CountryController@published');

    Route::get('/cities', 'CityController@index')->name('admin.cities.index');
    Route::get('/cities/show', 'CityController@show');
    Route::post('/cities/update', 'CityController@update');
    Route::post('/cities/store', 'CityController@store');
    Route::post('/cities/delete', 'CityController@delete');
    Route::post('/cities/published', 'CityController@published');

    Route::get('/tariffs', 'PriceController@index')->name('admin.tariffs.index');
    Route::get('/tariffs/show', 'PriceController@show');
    Route::get('/tariffs/show/compares/create', 'PriceController@showComparesCreate');
    Route::get('/tariffs/edit/{id}', 'PriceController@edit');
    Route::get('/tariffs/show/edit', 'PriceController@showEdit');
    Route::get('/tariffs/create', 'PriceController@create');
    Route::post('/tariffs/update', 'PriceController@update');
    Route::post('/tariffs/store', 'PriceController@store');
    Route::post('/tariffs/delete', 'PriceController@delete');
    Route::post('/tariffs/published', 'PriceController@published');

    Route::get('/compares', 'CompareController@index')->name('admin.compares.index');
    Route::get('/compares/show', 'CompareController@show');
    Route::post('/compares/update', 'CompareController@update');
    Route::post('/compares/store', 'CompareController@store');
    Route::post('/compares/delete', 'CompareController@delete');
    Route::post('/compares/published', 'CompareController@published');

    Route::get('/categories', 'CategoryController@index')->name('admin.categories.index');
    Route::get('/categories/show', 'CategoryController@show');
    Route::post('/categories/update', 'CategoryController@update');
    Route::post('/categories/store', 'CategoryController@store');
    Route::post('/categories/delete', 'CategoryController@delete');
    Route::post('/categories/published', 'CategoryController@published');

});


Route::get('/', 'HomeController@index')->name('home');

Route::get('login/{provider}', 'Auth\LoginController@redirectToProvider');
Route::get('login/{provider}/callback', 'Auth\LoginController@handleProviderCallback');

Route::group(['namespace' => 'Front'], function () {
    Route::get('/about', 'AboutController@index')->name('about');
    Route::get('/contacts', 'ContactsController@index')->name('contacts');
    Route::post('/contacts', 'ContactsController@send')->name('contacts.send');
    Route::get('/reviews', 'ReviewsController@index')->name('reviews');
    Route::post('/reviews', 'ReviewsController@store')->name('reviews.store');
    Route::get('/price', 'PriceController@index')->name('price');
    Route::get('/promocode', 'PromocodeController@index')->name('promocode');
    Route::get('/service', 'ServiceController@index')->name('service');
    Route::get('/payments', 'PaymentController@index')->name('payments');
    Route::post('/payments', 'PaymentController@payment')->name('payments.pay');
    Route::get('/success', 'PaymentController@success')->name('payments.success');
});


Route::group(['namespace' => 'Profile','prefix' => 'profile','middleware' => ['auth']], function () {
    Route::get('/', 'ProfileController@index')->name('profile');
    Route::post('/me/update', 'ProfileController@update')->name('profile.update');
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
    Route::get('/business', 'BusinessController@index')->name('business');
    Route::post('/business/delete', 'CompanyController@delete');
    Route::get('/add-company', 'CompanyController@create')->name('add-company');
    Route::post('/add-company', 'CompanyController@store')->name('company.store');
    Route::get('/edit-company/{id}', 'CompanyController@edit')->name('edit-company');
    Route::post('/edit-company/{id}', 'CompanyController@update')->name('company.update');
    Route::get('/checkout', 'CheckoutController@index')->name('checkout');
    Route::get('/logout', 'ProfileController@logout')->name('logout');
});
