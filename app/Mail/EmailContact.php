<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EmailContact extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $name;
    public $email;
    public $messageemail;

    public function __construct($name,$email,$messageemail)
    {
        $this->name = $name;
        $this->email = $email;
        $this->messageemail = $messageemail;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('info@nastroidom.ru')->subject('Форма обратной связи')->view('mail.contact');
    }
}
