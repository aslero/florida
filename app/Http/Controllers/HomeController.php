<?php

namespace App\Http\Controllers;

use App\Model\Content;
use App\Model\Faq;
use App\Model\Report;
use App\Model\Review;
use App\Model\Service;
use App\Model\Slider;
use Illuminate\Http\Request;

class HomeController extends Controller
{
     public function index()
    {
        $content = Content::where('slug','=','index')->first();
        $slider = Slider::where('published','=', 1)->orderBy('id','desc')->first();
        $reports = Report::orderBy('sortorder','asc')->get();
        $faqs = Faq::where('published','=',1)->get();
        $services = Service::orderBy('sortorder','asc')->get();
        $reviews = Review::where('published','=',1)->get();

    	return view('pages.index',[
    	    'content' => $content,
    	    'slider' => $slider,
    	    'reports' => $reports,
    	    'faqs' => $faqs,
    	    'services' => $services,
    	    'reviews' => $reviews,
        ]);
    }
}
