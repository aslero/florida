<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Model\Payment;
use App\Model\Price;
use App\Providers\RouteServiceProvider;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */
    public function showRegistrationForm(Request $request)
    {
        $tariff = 0;
        if ($request->isMethod('GET')){
            if ($request->tariff){
                $price = Price::where('id','=',$request->tariff)->first();
                if ($price->price > 0) $tariff = $price->id;
            }
        }
        return view('auth.register', [
            'tariff' => $tariff
        ]);
    }


    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::PROFILE;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'fullname' => ['string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6'],
            'g-recaptcha-response' => ['required'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)

    {
        $create = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'fullname' => $data['fullname'],
            'pass' => $data['password'],
            'password' => Hash::make($data['password']),
            'domain_id' => $this->getDomainActive(),
        ]);

        if ($create){
            if ($data['tariff'] == 0){
                $tariff = Price::where('price','=', 0)->where('published','=',1)->first();
            }else{
                $tariff = Price::where('id','=', $data['tariff'])->where('published','=',1)->first();
            }

            if ($tariff->days == 0) $date_to = Carbon::now()->addDays(365)->format('Y-m-d');else $date_to = Carbon::now()->addDays($tariff->days)->format('Y-m-d');
            if ($tariff){
                Payment::create([
                    'user_id' => $create->id,
                    'cost' => 0,
                    'tariff' => $tariff->id,
                    'count_company' => 10,
                    'status' => 0,
                    'arhive' => 0,
                    'chargeid' => null,
                    'date_to' => $date_to,
                    'domain_id' => $this->getDomainActive(),
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);
            }
        }

        return $create;
    }
}
