<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Model\Domain;
use App\Model\Payment;
use App\Model\Price;
use App\Model\Provider;
use App\Providers\RouteServiceProvider;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    public function validateLogin(Request $request)
    {
        $this->validate($request, [
            'g-recaptcha-response' => 'required|captcha',
        ]);
    }

    use AuthenticatesUsers;


    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::PROFILE;


    /**
     * Login username to be used by the controller.
     *
     * @var string
     */
    protected $username;

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');

        $this->username = $this->findUsername();
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function findUsername()
    {
        $login = request()->input('login');

        $fieldType = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'email' : 'name';

        request()->merge([$fieldType => $login]);

        return $fieldType;
    }

    /**
     * Get username property.
     *
     * @return string
     */
    public function username()
    {
        return $this->username;
    }

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($provider)
    {
        $login = '';
        $providerUser = Socialite::driver($provider)->user();


        $user = User::where('email','=', $providerUser->email)->first();
        if ($user){
            $userProvider = Provider::where('user_id','=',$user->id)
                            ->where('provider_id','=', $providerUser->id)
                            ->first();
            if (!$userProvider){
                Provider::create([
                    'user_id' => $user->id,
                    'provider_id' => $providerUser->getId(),
                    'provider' => $provider,
                    'nickname' => $providerUser->getNickname(),
                    'name' => $providerUser->getName(),
                    'email' => $providerUser->getEmail(),
                    'avatar' => $providerUser->getAvatar(),
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);
            }

        }else{
            $domainId = Domain::where('domain','=', env('DOMAIN'))->first();
            if (empty($providerUser->getNickname())) $login = $providerUser->getEmail();else $login = $providerUser->getNickname();
            $user = User::create([
                'name' => $login,
                'email' => $providerUser->getEmail(),
                'fullname' => $providerUser->getName(),
                'role_id' => 1,
                'ip' => $_SERVER['REMOTE_ADDR'],
                'domain_id' => $domainId->id,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);

            if ($user){
                Provider::create([
                    'user_id' => $user->id,
                    'provider_id' => $providerUser->getId(),
                    'provider' => $provider,
                    'nickname' => $providerUser->getNickname(),
                    'name' => $providerUser->getName(),
                    'email' => $providerUser->getEmail(),
                    'avatar' => $providerUser->getAvatar(),
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);
                $tariff = Price::where('price','=',0)->where('published','=',1)->first();
                if ($tariff){
                    Payment::create([
                        'user_id' => $user->id,
                        'cost' => 0,
                        'tariff' => $tariff->id,
                        'count_company' => 10,
                        'status' => 1,
                        'arhive' => 0,
                        'chargeid' => null,
                        'date_to' => Carbon::now()->addDays(365),
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now()
                    ]);
                }
            }
        }
        Auth::login($user,true);

        return redirect($this->redirectTo);
    }

}
