<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Review;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ReviewController extends Controller
{
    public function index(){
        return view('admin.pages.reviews');
    }

    public function show(){
        $reviews = Review::orderBy('id', 'desc')->where('domain_id','=',$this->getDomainActive())->get();

        return response()->json($reviews);
    }

    public function search(Request $request){
        if (isset($request->search)){
            $reviews = Review::where('description','like', '%'.$request->search. '%')
                ->where('domain_id','=',$this->getDomainActive())
                ->orderBy('reviews.id', 'desc')
                ->get();
        }else{
            $reviews = Review::orderBy('id', 'desc')->get();
        }

        return response()->json($reviews);
    }

    public function published(Request $request){
        if ($request->isMethod('post')){
            if (Auth::check()){

                $published = Review::where('id', '=', $request->id)->first()->update([
                    'published' => $request->type
                ]);

                if ($request->type == 0){
                    if ($published) return response()->json([
                        'error' => 0,
                        'message' => "Discontinued!"
                    ]);else return response()->json([
                        'error' => 1,
                        'message' => "Oops! Something went wrong! Try again!"
                    ]);
                }else{
                    if ($published) return response()->json([
                        'error' => 0,
                        'message' => "Published!"
                    ]);else return response()->json([
                        'error' => 1,
                        'message' => "Oops! Something went wrong! Try again!"
                    ]);
                }

            }else return response()->json([
                'error' => 2,
                'link' => '/login'
            ]);
        }

    }

    public function delete(Request $request){
        if ($request->isMethod('post')){
            if (Auth::check()){

                if (!empty($request->id)){
                    $delete = Review::where('id','=', $request->id)->first()->delete();

                    if ($delete) return response()->json([
                        'error' => 0,
                        'message' => 'Successfully deleted!'
                    ]);else return response()->json([
                        'error' => 1,
                        'message' => 'Oops! Something went wrong! Try again!'
                    ]);
                }

            }else return response()->json([
                'error' => 2,
                'link' => '/login'
            ]);
        }
    }

    public function update(Request $request){
        if ($request->isMethod('post')){
            if (Auth::check()){
                $errors_m = '';
                $v = Validator::make($request->all(), [
                    'text' => ['required', 'string'],
                ]);

                if ($v->fails())
                {
                    $errors = $v->errors();
                    foreach ($errors->all() as $error){
                        $errors_m.= $error;
                    }
                    return response()
                        ->json([
                            'message' => $errors_m,
                            'error' => 1,
                        ], 200);
                }

                $store = Review::where('id','=', $request->id)->first()->update([
                    'text' => htmlspecialchars(trim($request->text)),
                    'domain_id' => $this->getDomainActive(),
                    'updated_at' => Carbon::now()
                ]);


                if ($store) return response()->json([
                    'error' => 0,
                    'review' => Review::where('id','=', $request->id)->first(),
                    'message' => 'Successfully updated!'
                ]);else return response()->json([
                    'error' => 1,
                    'message' => 'Oops! Something went wrong! Try again!'
                ]);

            }else return response()->json([
                'error' => 2,
                'link' => '/login'
            ]);
        }
    }
}
