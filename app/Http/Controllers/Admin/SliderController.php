<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class SliderController extends Controller
{
    public function index(){
        return view('admin.pages.slider');
    }

    public function show(){
        $sliders = Slider::orderBy('id', 'ASC')->where('domain_id','=',$this->getDomainActive())->get();

        return response()->json([
            'sliders' => $sliders
        ]);
    }

    public function edit(Request $request){
        if ($request->isMethod('post')){
            if (Auth::check()){
                $slider = Slider::where('id', '=', $request->id)->first();
                if ($slider) return response()->json([
                    'error' => 0,
                    'slider' => $slider
                ]);else return response()->json([
                    'error' => 1,
                    'message' => 'Nothing found!'
                ]);
            }
        }else return response()->json([
            'error' => 2,
            'link' => '/login'
        ]);
    }


    public function store(Request $request)
    {
        if ($request->isMethod('post')) {
            if (Auth::check()) {

                $slider = Slider::create([
                    'pagetitle' => htmlspecialchars(trim($request->pagetitle)),
                    'description' => htmlspecialchars(trim($request->description)),
                    'img' => $request->img,
                    'published' => 1,
                    'domain_id' => $this->getDomainActive(),
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);

                if ($slider) {
                    return response()->json([
                        'error' => 0,
                        'slider' => $slider
                    ]);
                } else return response()->json([
                    'error' => 1,
                    'message' => 'Oops! Something went wrong! Try again!'
                ]);

            } else return response()->json([
                'error' => 2,
                'link' => '/login'
            ]);
        }
    }

    public function update(Request $request)
    {
        if ($request->isMethod('post')) {
            if (Auth::check()) {
                $slider = Slider::where('id', '=', $request->id)->first()->update([
                    'pagetitle' => htmlspecialchars(trim($request->pagetitle)),
                    'description' => htmlspecialchars(trim($request->description)),
                    'img' => $request->img,
                    'domain_id' => $this->getDomainActive(),
                    'updated_at' => Carbon::now()
                ]);

                if ($slider) {
                    return response()->json([
                        'error' => 0,
                        'slider' => Slider::where('id', '=',  $request->id)->first()
                    ]);
                } else return response()->json([
                    'error' => 1,
                    'message' => 'Oops! Something went wrong! Try again!'
                ]);

            } else return response()->json([
                'error' => 2,
                'link' => '/login'
            ]);
        }
    }

    public function delete(Request $request){
        if ($request->isMethod('post')){
            if (Auth::check()){

                if (!empty($request->id)){
                    $delete = Slider::where('id','=', $request->id)->first()->delete();

                    if ($delete) return response()->json([
                        'error' => 0,
                        'message' => 'Successfully deleted!'
                    ]);else return response()->json([
                        'error' => 1,
                        'message' => 'Oops! Something went wrong! Try again!'
                    ]);
                }

            }else return response()->json([
                'error' => 2,
                'link' => '/login'
            ]);
        }
    }

    public function published(Request $request){
        if ($request->isMethod('post')){
            if (Auth::check()){

                $published = Slider::where('id', '=', $request->id)->first()->update([
                    'published' => $request->type
                ]);

                if ($request->type == 0){
                    if ($published) return response()->json([
                        'error' => 0,
                        'message' => "Discontinued!"
                    ]);else return response()->json([
                        'error' => 1,
                        'message' => "Oops! Something went wrong! Try again!"
                    ]);
                }else{
                    if ($published) return response()->json([
                        'error' => 0,
                        'message' => "Published!"
                    ]);else return response()->json([
                        'error' => 1,
                        'message' => "Oops! Something went wrong! Try again!"
                    ]);
                }

            }else return response()->json([
                'error' => 2,
                'link' => '/login'
            ]);
        }

    }
}
