<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class CountryController extends Controller
{
    public function index(){
        return view('admin.pages.countries');
    }

    public function show(){
        $countries = Country::orderBy('id', 'ASC')->get();

        return response()->json($countries);
    }


    public function store(Request $request)
    {
        if ($request->isMethod('post')) {
            if (Auth::check()) {

                $store = Country::create([
                    'title' => htmlspecialchars(trim($request->title)),
                    'description' => htmlspecialchars(trim($request->description)),
                    'published' => 1,
                    'slug' => Str::slug(htmlspecialchars(trim($request->title)),'-'),
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);

                if ($store) {
                    return response()->json([
                        'error' => 0,
                        'country' => $store
                    ]);
                } else return response()->json([
                    'error' => 1,
                    'message' => 'Oops! Something went wrong! Try again!'
                ]);

            } else return response()->json([
                'error' => 2,
                'link' => '/login'
            ]);
        }
    }

    public function update(Request $request)
    {
        if ($request->isMethod('post')) {
            if (Auth::check()) {
                $store = Country::where('id', '=', $request->id)->first()->update([
                    'title' => htmlspecialchars(trim($request->title)),
                    'description' => htmlspecialchars(trim($request->description)),
                    'slug' => Str::slug(htmlspecialchars(trim($request->title)),'-'),
                    'updated_at' => Carbon::now()
                ]);

                if ($store) {
                    return response()->json([
                        'error' => 0,
                        'country' => Country::where('id', '=',  $request->id)->first()
                    ]);
                } else return response()->json([
                    'error' => 1,
                    'message' => 'Oops! Something went wrong! Try again!'
                ]);

            } else return response()->json([
                'error' => 2,
                'link' => '/login'
            ]);
        }
    }

    public function delete(Request $request){
        if ($request->isMethod('post')){
            if (Auth::check()){

                if (!empty($request->id)){
                    $delete = Country::where('id','=', $request->id)->first()->delete();

                    if ($delete) return response()->json([
                        'error' => 0,
                        'message' => 'Successfully deleted!'
                    ]);else return response()->json([
                        'error' => 1,
                        'message' => 'Oops! Something went wrong! Try again!'
                    ]);
                }

            }else return response()->json([
                'error' => 2,
                'link' => '/login'
            ]);
        }
    }

    public function published(Request $request){
        if ($request->isMethod('post')){
            if (Auth::check()){

                $published = Country::where('id', '=', $request->id)->first()->update([
                    'published' => $request->type
                ]);

                if ($request->type == 0){
                    if ($published) return response()->json([
                        'error' => 0,
                        'message' => "Discontinued!"
                    ]);else return response()->json([
                        'error' => 1,
                        'message' => "Oops! Something went wrong! Try again!"
                    ]);
                }else{
                    if ($published) return response()->json([
                        'error' => 0,
                        'message' => "Published!"
                    ]);else return response()->json([
                        'error' => 1,
                        'message' => "Oops! Something went wrong! Try again!"
                    ]);
                }

            }else return response()->json([
                'error' => 2,
                'link' => '/login'
            ]);
        }

    }
}
