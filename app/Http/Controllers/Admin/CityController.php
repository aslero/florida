<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\City;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CityController extends Controller
{
    public function index(){
        return view('admin.pages.cities');
    }

    public function show(){
        $cities = DB::table('cities')
            ->leftJoin('countries','countries.id','=','cities.country_id')
            ->select('countries.title as country','cities.title','cities.country_id','cities.published','cities.id','cities.rating')
            ->get();

        return response()->json($cities);
    }


    public function store(Request $request)
    {
        if ($request->isMethod('post')) {
            if (Auth::check()) {

                $store = City::create([
                    'title' => htmlspecialchars(trim($request->title)),
                    'country_id' => $request->country_id,
                    'rating' => $request->rating,
                    'published' => 1,
                    'slug' => Str::slug(htmlspecialchars(trim($request->title)),'-'),
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);

                if ($store) {
                    $citiy = DB::table('cities')
                        ->leftJoin('countries','countries.id','=','cities.country_id')
                        ->select('countries.title as country','cities.title','cities.country_id','cities.published','cities.id','cities.rating')
                        ->where('cities.id','=', $store->id)->first();

                    return response()->json([
                        'error' => 0,
                        'city' => $citiy
                    ]);
                } else return response()->json([
                    'error' => 1,
                    'message' => 'Oops! Something went wrong! Try again!'
                ]);

            } else return response()->json([
                'error' => 2,
                'link' => '/login'
            ]);
        }
    }

    public function update(Request $request)
    {
        if ($request->isMethod('post')) {
            if (Auth::check()) {
                $store = City::where('id', '=', $request->id)->first()->update([
                    'title' => htmlspecialchars(trim($request->title)),
                    'country_id' => $request->country_id,
                    'rating' => $request->rating,
                    'slug' => Str::slug(htmlspecialchars(trim($request->title)),'-'),
                    'updated_at' => Carbon::now()
                ]);

                if ($store) {
                    return response()->json([
                        'error' => 0,
                        'city' => City::where('id', '=',  $request->id)->first()
                    ]);
                } else return response()->json([
                    'error' => 1,
                    'message' => 'Oops! Something went wrong! Try again!'
                ]);

            } else return response()->json([
                'error' => 2,
                'link' => '/login'
            ]);
        }
    }

    public function delete(Request $request){
        if ($request->isMethod('post')){
            if (Auth::check()){

                if (!empty($request->id)){
                    $delete = City::where('id','=', $request->id)->first()->delete();

                    if ($delete) return response()->json([
                        'error' => 0,
                        'message' => 'Successfully deleted!'
                    ]);else return response()->json([
                        'error' => 1,
                        'message' => 'Oops! Something went wrong! Try again!'
                    ]);
                }

            }else return response()->json([
                'error' => 2,
                'link' => '/login'
            ]);
        }
    }

    public function published(Request $request){
        if ($request->isMethod('post')){
            if (Auth::check()){

                $published = City::where('id', '=', $request->id)->first()->update([
                    'published' => $request->type
                ]);

                if ($request->type == 0){
                    if ($published) return response()->json([
                        'error' => 0,
                        'message' => "Discontinued!"
                    ]);else return response()->json([
                        'error' => 1,
                        'message' => "Oops! Something went wrong! Try again!"
                    ]);
                }else{
                    if ($published) return response()->json([
                        'error' => 0,
                        'message' => "Опубликован!"
                    ]);else return response()->json([
                        'error' => 1,
                        'message' => "Oops! Something went wrong! Try again!"
                    ]);
                }

            }else return response()->json([
                'error' => 2,
                'link' => '/login'
            ]);
        }

    }
}
