<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class CategoryController extends Controller
{
    public function index(){
        return view('admin.pages.categories');
    }

    public function show(){
        $categories = Category::orderBy('id','asc')->get();

        return response()->json($categories);
    }


    public function store(Request $request)
    {
        if ($request->isMethod('post')) {
            if (Auth::check()) {

                $store = Category::create([
                    'title' => htmlspecialchars(trim($request->title)),
                    'published' => 1,
                    'slug' => Str::slug(htmlspecialchars(trim($request->title)),'-'),
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);

                if ($store) {
                    return response()->json([
                        'error' => 0,
                        'category' => $store
                    ]);
                } else return response()->json([
                    'error' => 1,
                    'message' => 'Oops! Something went wrong! Try again!'
                ]);

            } else return response()->json([
                'error' => 2,
                'link' => '/login'
            ]);
        }
    }

    public function update(Request $request)
    {
        if ($request->isMethod('post')) {
            if (Auth::check()) {
                $store = Category::where('id', '=', $request->id)->first()->update([
                    'title' => htmlspecialchars(trim($request->title)),
                    'slug' => Str::slug(htmlspecialchars(trim($request->title)),'-'),
                    'updated_at' => Carbon::now()
                ]);

                if ($store) {
                    return response()->json([
                        'error' => 0,
                        'category' => Category::where('id', '=',  $request->id)->first()
                    ]);
                } else return response()->json([
                    'error' => 1,
                    'message' => 'Oops! Something went wrong! Try again!'
                ]);

            } else return response()->json([
                'error' => 2,
                'link' => '/login'
            ]);
        }
    }

    public function delete(Request $request){
        if ($request->isMethod('post')){
            if (Auth::check()){

                if (!empty($request->id)){
                    $delete = Category::where('id','=', $request->id)->first()->delete();

                    if ($delete) return response()->json([
                        'error' => 0,
                        'message' => 'Successfully deleted!'
                    ]);else return response()->json([
                        'error' => 1,
                        'message' => 'Oops! Something went wrong! Try again!'
                    ]);
                }

            }else return response()->json([
                'error' => 2,
                'link' => '/login'
            ]);
        }
    }

    public function published(Request $request){
        if ($request->isMethod('post')){
            if (Auth::check()){

                $published = Category::where('id', '=', $request->id)->first()->update([
                    'published' => $request->type
                ]);

                if ($request->type == 0){
                    if ($published) return response()->json([
                        'error' => 0,
                        'message' => "Discontinued!"
                    ]);else return response()->json([
                        'error' => 1,
                        'message' => "Oops! Something went wrong! Try again!"
                    ]);
                }else{
                    if ($published) return response()->json([
                        'error' => 0,
                        'message' => "Опубликован!"
                    ]);else return response()->json([
                        'error' => 1,
                        'message' => "Oops! Something went wrong! Try again!"
                    ]);
                }

            }else return response()->json([
                'error' => 2,
                'link' => '/login'
            ]);
        }

    }
}
