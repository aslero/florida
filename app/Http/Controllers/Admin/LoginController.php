<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    public function index(){
        return view('admin.auth.login');
    }

    public function login(Request $request){
        $errors_m = '';


        $v = Validator::make($request->all(), [
            'email' => ['required', 'string', 'email', 'max:255'],
            'password' => ['required', 'string', 'min:6']
        ]);

        if ($v->fails())
        {
            $errors = $v->errors();
            foreach ($errors->all() as $error){
                $errors_m.= $error;
            }
            return response()
                ->json([
                    'message' => $errors_m,
                    'error' => 1,
                ], 200);
        }

        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){
            $this->getDomainActive();
            return response()->json([
                'error' => 0,
                'link' => '/admin'
            ]);
        }else return response()->json([
            'error' => 1,
            'message' => 'Oops! Something went wrong! Try again!'
        ]);

    }

    public function logout(){
        Auth::logout();
        return redirect()->route('admin.login');
    }
}
