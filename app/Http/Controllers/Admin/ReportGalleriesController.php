<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Report;
use App\Model\ReportGallery;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ReportGalleriesController extends Controller
{
    public function index($id){
        $report = Report::where('id', '=', $id)->first();
        return view('admin.pages.reports-galleries',[
            'report' => $report
        ]);
    }

    public function show(Request $request){
        $stats = ReportGallery::where('report_id','=', $request->id)->get();

        return response()->json($stats);
    }


    public function delete(Request $request){
        if ($request->isMethod('post')){
            if (Auth::check()){

                if (!empty($request->id)){
                    $delete = ReportGallery::where('id','=', $request->id)->first()->delete();

                    if ($delete) return response()->json([
                        'error' => 0,
                        'message' => 'Successfully deleted!'
                    ]);else return response()->json([
                        'error' => 1,
                        'message' => 'Oops! Something went wrong! Try again!'
                    ]);
                }

            }else return response()->json([
                'error' => 2,
                'link' => '/login'
            ]);
        }
    }

    public function published(Request $request){
        if ($request->isMethod('post')){
            if (Auth::check()){

                $published = ReportGallery::where('id', '=', $request->id)->first()->update([
                    'published' => $request->type
                ]);

                if ($request->type == 0){
                    if ($published) return response()->json([
                        'error' => 0,
                        'message' => "Discontinued!"
                    ]);else return response()->json([
                        'error' => 1,
                        'message' => "Oops! Something went wrong! Try again!"
                    ]);
                }else{
                    if ($published) return response()->json([
                        'error' => 0,
                        'message' => "Published!"
                    ]);else return response()->json([
                        'error' => 1,
                        'message' => "Oops! Something went wrong! Try again!"
                    ]);
                }

            }else return response()->json([
                'error' => 2,
                'link' => '/login'
            ]);
        }

    }

    public function update(Request $request){
        if ($request->isMethod('post')){
            if (Auth::check()){
                $errors_m = '';
                $v = Validator::make($request->all(), [
                    'image' => ['required'],
                    'description' => ['required', 'string','max:255'],
                ]);

                if ($v->fails())
                {
                    $errors = $v->errors();
                    foreach ($errors->all() as $error){
                        $errors_m.= $error;
                    }
                    return response()
                        ->json([
                            'message' => $errors_m,
                            'error' => 1,
                        ], 200);
                }

                $store = ReportGallery::where('id','=',$request->id)->first()->update([
                    'image' => trim($request->image),
                    'description' => trim($request->description),
                    'updated_at' => Carbon::now()
                ]);


                if ($store) return response()->json([
                    'error' => 0,
                    'gallery' => ReportGallery::where('id','=', $request->id)->first(),
                    'message' => 'Усрешно изменено!'
                ]);else return response()->json([
                    'error' => 1,
                    'message' => 'Не удалось изменить! Попробуйте еще раз!'
                ]);

            }else return response()->json([
                'error' => 2,
                'link' => '/login'
            ]);
        }
    }

    public function store(Request $request){
        if ($request->isMethod('post')){
            if (Auth::check()){
                $errors_m = '';
                $v = Validator::make($request->all(), [
                    'image' => ['required'],
                    'description' => ['required', 'string','max:255'],
                ]);

                if ($v->fails())
                {
                    $errors = $v->errors();
                    foreach ($errors->all() as $error){
                        $errors_m.= $error;
                    }
                    return response()
                        ->json([
                            'message' => $errors_m,
                            'error' => 1,
                        ], 200);
                }

                $store = ReportGallery::create([
                    'image' => trim($request->image),
                    'description' => trim($request->description),
                    'report_id' => $request->report,
                    'published' => 1,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);


                if ($store) return response()->json([
                    'error' => 0,
                    'gallery' => $store,
                    'message' => 'Успешно добавлено!'
                ]);else return response()->json([
                    'error' => 1,
                    'message' => 'Не удалось добавить! Попробуйте еще раз!'
                ]);

            }else return response()->json([
                'error' => 2,
                'link' => '/login'
            ]);
        }
    }
}
