<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Payment;
use Cartalyst\Stripe\Stripe;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PaymentsController extends Controller
{
    public function index(){
        return view('admin.pages.payments');
    }

    public function refund(Request $request){

        $payment = Payment::where('id','=', $request->id)->first();

        $stripe = Stripe::make(env('STRIPE_SECRET'));
        try {
            $refund = $stripe->refunds()->create($payment->chargeid);


            if($refund['status'] == 'succeeded') {
                $payment->update([
                    'status' => 3,
                    'updated_at' => Carbon::now()
                ]);

                return response()->json([
                    'error' => 0,
                    'message' => 'Money successfully returned!'
                ]);

            }else {
                return response()->json([
                    'error' => 1,
                    'message' => 'Failed to return funds!'
                ]);
            }
        } catch (Exception $e) {
            return response()->json([
                'error' => 1,
                'message' => $e->getMessage()
            ]);
        } catch(\Cartalyst\Stripe\Exception\CardErrorException $e) {
            return response()->json([
                'error' => 1,
                'message' => $e->getMessage()
            ]);
        } catch(\Cartalyst\Stripe\Exception\MissingParameterException $e) {
            return response()->json([
                'error' => 1,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function show(){
        $payments = DB::table('payments')
                        ->leftJoin('users','users.id','=', 'payments.user_id')
                        ->select('users.name','users.email','users.fullname','users.phone','payments.*')
                        ->where('payments.arhive','=', 0)
                        ->where('payments.domain_id','=',$this->getDomainActive())
                        ->orderBy('id','desc')->get();

        return response()->json($payments);
    }

    public function getArchive(){
        $payments = DB::table('payments')
                        ->leftJoin('users','users.id','=', 'payments.user_id')
                        ->select('users.name','users.email','users.fullname','users.phone','payments.*')
                        ->where('payments.domain_id','=',$this->getDomainActive())
                        ->where('payments.arhive','=', 1)
                        ->orderBy('id','desc')->get();

        return response()->json($payments);
    }

    public function filter(Request $request){
        if ($request->isMethod('GET')) {

            $payments = Payment::with('user');

            if ($request->has('fullname') && !empty($request->fullname)){
                $payments = $payments->whereHas('user', function ($query) use ($request){
                    $query->where('fullname','like', '%'.$request->fullname. '%');
                });
            }

            if ($request->has('email') && !empty($request->email)){
                $payments = $payments->whereHas('user', function ($query) use ($request){
                    $query->where('email','like', '%'.$request->email. '%');
                });
            }

            if ($request->has('date') && !empty($request->date)){
                $payments = $payments->whereDate('created_at',Carbon::parse($request->date)->format('Y-m-d'));
            }

            $payments = $payments->where('arhive','=',$request->arhive);
            $payments = $payments->where('domain_id','=',$this->getDomainActive());

            $payments = $payments->orderBy('id','desc')
                ->get();


            foreach ($payments as $payment) {
                $payment->name = $payment->user->name;
                $payment->email = $payment->user->email;
                $payment->fullname = $payment->user->fullname;
                $payment->phone = $payment->user->phone;
            }

            return response()->json($payments);
        }
    }

    public function archive(Request $request){
        if ($request->isMethod('post')){
            if (Auth::check()){

                if (!empty($request->id)){
                    $archive = Payment::where('id','=', $request->id)->first()->update([
                        'arhive' => 1
                    ]);

                    if ($archive) return response()->json([
                        'error' => 0,
                        'message' => 'Payment transferred to archive!'
                    ]);else return response()->json([
                        'error' => 1,
                        'message' => 'Oops! Something went wrong! Try again!'
                    ]);
                }

            }else return response()->json([
                'error' => 2,
                'link' => '/login'
            ]);
        }
    }

    public function reestablish(Request $request){
        if ($request->isMethod('post')){
            if (Auth::check()){

                if (!empty($request->id)){
                    $archive = Payment::where('id','=', $request->id)->first()->update([
                        'arhive' => 0
                    ]);

                    if ($archive) return response()->json([
                        'error' => 0,
                        'message' => 'Payment transferred to archive!'
                    ]);else return response()->json([
                        'error' => 1,
                        'message' => 'Oops! Something went wrong! Try again!'
                    ]);
                }

            }else return response()->json([
                'error' => 2,
                'link' => '/login'
            ]);
        }
    }
    public function delete(Request $request){
        if ($request->isMethod('post')){
            if (Auth::check()){

                if (!empty($request->id)){
                    $delete = Payment::where('id','=', $request->id)->first()->delete();

                    if ($delete) return response()->json([
                        'error' => 0,
                        'message' => 'Successfully deleted!'
                    ]);else return response()->json([
                        'error' => 1,
                        'message' => 'Oops! Something went wrong! Try again!'
                    ]);
                }

            }else return response()->json([
                'error' => 2,
                'link' => '/login'
            ]);
        }
    }
}
