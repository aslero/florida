<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Compare;
use App\Model\Price;
use App\Model\PriceCompare;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PriceController extends Controller
{
    public function index(){
        return view('admin.pages.tariffs');
    }

    public function show(){
        $prices = Price::orderBy('id', 'ASC')->get();

        return response()->json($prices);
    }

    public function create(){
        return view('admin.forms.tariff-create');
    }

    public function edit($id){
        return view('admin.forms.tariff-edit',[
            'id' => $id
        ]);
    }

    public function showEdit(Request $request){
        $tariff = Price::where('id','=', $request->id)->first();
        $compares = DB::table('price_compares')
                        ->leftJoin('compares','compares.id','=','price_compares.compare_id')
                        ->select('compares.title','price_compares.availability','price_compares.comment','price_compares.id')
                        ->where('price_compares.price_id','=', $request->id)->get();

        return response()->json([
            'tariff' => $tariff,
            'compares' => $compares,
        ]);
    }

    public function showComparesCreate(){
        $compares = Compare::orderBy('id','asc')->get();

        foreach ($compares as $compare) {
            $compare->availability = 0;
            $compare->comment = null;
        }

        return response()->json($compares);
    }

    public function store(Request $request)
    {
        if ($request->isMethod('post')) {
            if (Auth::check()) {

                $store = Price::create([
                    'title' => htmlspecialchars(trim($request->title)),
                    'price' => $request->price,
                    'days' => $request->days,
                    'published' => $request->published,
                    'special' => $request->special,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);

                if ($store) {
                    if ($request->compares){
                        foreach (json_decode($request->compares) as $compare) {
                            $storeCompare = PriceCompare::create([
                                'price_id' => $store->id,
                                'compare_id' => $compare->id,
                                'availability' => $compare->availability,
                                'comment' => $compare->comment,
                                'created_at' => Carbon::now(),
                                'updated_at' => Carbon::now()
                            ]);
                        }
                    }
                    return response()->json([
                        'error' => 0,
                        'message' => 'Successfully created'
                    ]);
                } else return response()->json([
                    'error' => 1,
                    'message' => 'Oops! Something went wrong! Try again!'
                ]);

            } else return response()->json([
                'error' => 2,
                'link' => '/login'
            ]);
        }
    }

    public function update(Request $request)
    {
        if ($request->isMethod('post')) {
            if (Auth::check()) {
                $update = Price::where('id','=',$request->id)->first()->update([
                    'title' => htmlspecialchars(trim($request->title)),
                    'price' => $request->price,
                    'days' => $request->days,
                    'published' => $request->published,
                    'special' => $request->special,
                    'updated_at' => Carbon::now()
                ]);

                if ($update) {
                    if ($request->compares){
                        foreach (json_decode($request->compares) as $compare) {
                            $storeCompare = PriceCompare::where('id','=', $compare->id)->first()->update([
                                'availability' => $compare->availability,
                                'comment' => $compare->comment,
                                'updated_at' => Carbon::now()
                            ]);
                        }
                    }
                    return response()->json([
                        'error' => 0,
                        'message' => 'Successfully updated'
                    ]);
                } else return response()->json([
                    'error' => 1,
                    'message' => 'Oops! Something went wrong! Try again!'
                ]);

            } else return response()->json([
                'error' => 2,
                'link' => '/login'
            ]);
        }
    }

    public function published(Request $request){
        if ($request->isMethod('post')){
            if (Auth::check()){

                $published = Price::where('id', '=', $request->id)->first()->update([
                    'published' => $request->type
                ]);

                if ($request->type == 0){
                    if ($published) return response()->json([
                        'error' => 0,
                        'message' => "Discontinued!"
                    ]);else return response()->json([
                        'error' => 1,
                        'message' => "Oops! Something went wrong! Try again!"
                    ]);
                }else{
                    if ($published) return response()->json([
                        'error' => 0,
                        'message' => "Опубликован!"
                    ]);else return response()->json([
                        'error' => 1,
                        'message' => "Oops! Something went wrong! Try again!"
                    ]);
                }

            }else return response()->json([
                'error' => 2,
                'link' => '/login'
            ]);
        }

    }

    public function delete(Request $request){
        if ($request->isMethod('post')){
            if (Auth::check()){

                if (!empty($request->id)){
                    $delete = Price::where('id','=', $request->id)->first()->delete();

                    if ($delete) return response()->json([
                        'error' => 0,
                        'message' => 'Successfully deleted!'
                    ]);else return response()->json([
                        'error' => 1,
                        'message' => 'Oops! Something went wrong! Try again!'
                    ]);
                }

            }else return response()->json([
                'error' => 2,
                'link' => '/login'
            ]);
        }
    }
}
