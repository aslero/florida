<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Compare;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class CompareController extends Controller
{
    public function index(){
        return view('admin.pages.compares');
    }

    public function show(){
        $compares = Compare::orderBy('id', 'ASC')->get();

        return response()->json($compares);
    }

    public function store(Request $request)
    {
        if ($request->isMethod('post')) {
            if (Auth::check()) {

                $store = Compare::create([
                    'title' => htmlspecialchars(trim($request->title)),
                    'description' => htmlspecialchars(trim($request->description)),
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);

                if ($store) {
                    return response()->json([
                        'error' => 0,
                        'compare' => $store
                    ]);
                } else return response()->json([
                    'error' => 1,
                    'message' => 'Oops! Something went wrong! Try again!'
                ]);

            } else return response()->json([
                'error' => 2,
                'link' => '/login'
            ]);
        }
    }

    public function update(Request $request)
    {
        if ($request->isMethod('post')) {
            if (Auth::check()) {
                $store = Compare::where('id', '=', $request->id)->first()->update([
                    'title' => htmlspecialchars(trim($request->title)),
                    'description' => htmlspecialchars(trim($request->description)),
                    'updated_at' => Carbon::now()
                ]);

                if ($store) {
                    return response()->json([
                        'error' => 0,
                        'compare' => Compare::where('id', '=',  $request->id)->first()
                    ]);
                } else return response()->json([
                    'error' => 1,
                    'message' => 'Oops! Something went wrong! Try again!'
                ]);

            } else return response()->json([
                'error' => 2,
                'link' => '/login'
            ]);
        }
    }

    public function delete(Request $request){
        if ($request->isMethod('post')){
            if (Auth::check()){

                if (!empty($request->id)){
                    $delete = Compare::where('id','=', $request->id)->first()->delete();

                    if ($delete) return response()->json([
                        'error' => 0,
                        'message' => 'Successfully deleted!'
                    ]);else return response()->json([
                        'error' => 1,
                        'message' => 'Oops! Something went wrong! Try again!'
                    ]);
                }

            }else return response()->json([
                'error' => 2,
                'link' => '/login'
            ]);
        }
    }
}
