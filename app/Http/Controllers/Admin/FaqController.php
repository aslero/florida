<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Faq;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class FaqController extends Controller
{
    public function index(){
        return view('admin.pages.faqs');
    }

    public function show(){
        $reviews = Faq::where('domain_id','=',$this->getDomainActive())->orderBy('id', 'desc')->get();

        return response()->json($reviews);
    }

    public function search(Request $request){
        if (isset($request->search)){
            $reviews = Faq::where('description','like', '%'.$request->search. '%')
                ->where('domain_id','=',$this->getDomainActive())
                ->orderBy('id', 'desc')
                ->get();
        }else{
            $reviews = Faq::orderBy('id', 'desc')->get();
        }

        return response()->json($reviews);
    }

    public function published(Request $request){
        if ($request->isMethod('post')){
            if (Auth::check()){

                $published = Faq::where('id', '=', $request->id)->first()->update([
                    'published' => $request->type
                ]);

                if ($request->type == 0){
                    if ($published) return response()->json([
                        'error' => 0,
                        'message' => "Discontinued!"
                    ]);else return response()->json([
                        'error' => 1,
                        'message' => "Successfully deleted!"
                    ]);
                }else{
                    if ($published) return response()->json([
                        'error' => 0,
                        'message' => "Published!"
                    ]);else return response()->json([
                        'error' => 1,
                        'message' => "Successfully deleted!"
                    ]);
                }

            }else return response()->json([
                'error' => 2,
                'link' => '/login'
            ]);
        }

    }

    public function delete(Request $request){
        if ($request->isMethod('post')){
            if (Auth::check()){

                if (!empty($request->id)){
                    $delete = Faq::where('id','=', $request->id)->first()->delete();

                    if ($delete) return response()->json([
                        'error' => 0,
                        'message' => 'Successfully deleted!'
                    ]);else return response()->json([
                        'error' => 1,
                        'message' => 'Oops! Something went wrong! Try again!'
                    ]);
                }

            }else return response()->json([
                'error' => 2,
                'link' => '/login'
            ]);
        }
    }

    public function update(Request $request){
        if ($request->isMethod('post')){
            if (Auth::check()){
                $errors_m = '';
                $v = Validator::make($request->all(), [
                    'title' => ['required', 'string','max:255'],
                    'description' => ['required', 'string'],
                ]);

                if ($v->fails())
                {
                    $errors = $v->errors();
                    foreach ($errors->all() as $error){
                        $errors_m.= $error;
                    }
                    return response()
                        ->json([
                            'message' => $errors_m,
                            'error' => 1,
                        ], 200);
                }

                $store = Faq::where('id','=', $request->id)->first()->update([
                    'title' => trim($request->title),
                    'description' => trim($request->description),
                    'icon' => trim($request->icon),
                    'domain_id' => $this->getDomainActive(),
                    'updated_at' => Carbon::now()
                ]);


                if ($store) return response()->json([
                    'error' => 0,
                    'faq' => Faq::where('id','=', $request->id)->first(),
                    'message' => 'Successfully updated!'
                ]);else return response()->json([
                    'error' => 1,
                    'message' => 'Oops! Something went wrong! Try again!'
                ]);

            }else return response()->json([
                'error' => 2,
                'link' => '/login'
            ]);
        }
    }

    public function store(Request $request){
        if ($request->isMethod('post')){
            if (Auth::check()){
                $errors_m = '';
                $v = Validator::make($request->all(), [
                    'title' => ['required', 'string','max:255'],
                    'description' => ['required', 'string'],
                ]);

                if ($v->fails())
                {
                    $errors = $v->errors();
                    foreach ($errors->all() as $error){
                        $errors_m.= $error;
                    }
                    return response()
                        ->json([
                            'message' => $errors_m,
                            'error' => 1,
                        ], 200);
                }

                $store = Faq::create([
                    'title' => trim($request->title),
                    'description' => trim($request->description),
                    'icon' => trim($request->icon),
                    'published' => 1,
                    'domain_id' => $this->getDomainActive(),
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);


                if ($store) return response()->json([
                    'error' => 0,
                    'faq' => $store,
                    'message' => 'Successfully added!'
                ]);else return response()->json([
                    'error' => 1,
                    'message' => 'Oops! Something went wrong! Try again!'
                ]);

            }else return response()->json([
                'error' => 2,
                'link' => '/login'
            ]);
        }
    }
}
