<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Content;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ContentController extends Controller
{
    public function index(){
        return view('admin.pages.pages');
    }

    public function show(){
        $pages = Content::orderBy('id', 'ASC')->get();

        return response()->json([
            'pages' => $pages
        ]);
    }

    public function search(Request $request){
        if (isset($request->search)){
            $pages = Content::where('pagetitle', 'like', '%'.$request->search. '%')
                ->orderBy('id', 'ASC')
                ->get();
        }else{
            $pages = Content::orderBy('id', 'ASC')->get();
        }

        return response()->json($pages);
    }

    public function edit($id){
        return view('admin.forms.page',[
            'id' => $id
        ]);
    }

    public function info(Request $request){
        return response()->json(Content::where('id','=', $request->id)->first());
    }

    public function update(Request $request, $id){
        if ($request->isMethod('post')){
            if (Auth::check()){
                $errors_m = '';
                $v = Validator::make($request->all(), [
                    'pagetitle' => ['required', 'string', 'max:255'],
                    'description' => ['string', 'max:255'],
                    'meta_pagetitle' => ['string', 'max:255'],
                    'meta_description' => ['string', 'max:255'],
                ]);

                if ($v->fails())
                {
                    $errors = $v->errors();
                    foreach ($errors->all() as $error){
                        $errors_m.= $error;
                    }
                    return response()
                        ->json([
                            'message' => $errors_m,
                            'error' => 1,
                        ], 200);
                }



                $article = Content::where('id','=',$id)->first()->update([
                    'pagetitle' => htmlspecialchars(trim($request->pagetitle)),
                    'description' => htmlspecialchars(trim($request->description)),
                    'meta_pagetitle' => htmlspecialchars(trim($request->meta_pagetitle)),
                    'meta_keywords' => htmlspecialchars(trim($request->meta_keywords)),
                    'meta_description' => htmlspecialchars(trim($request->meta_description)),
                    'content' => trim($request->content),
                    'img' => $request->img,
                    'published' => $request->published,
                    'hidemenu' => $request->hidemenu,
                    'menuindex' => $request->menuindex,
                    'bottom' => $request->bottom,
                    'updated_at' => Carbon::now()
                ]);

                if ($article){
                    return response()->json([
                        'error' => 0,
                        'message' => 'Page successfully changed!'
                    ]);
                }else return response()->json([
                    'error' => 1,
                    'message' => 'Oops! Something went wrong! Try again!'
                ]);

            }else return response()->json([
                'error' => 2,
                'link' => '/login'
            ]);
        }

    }
}
