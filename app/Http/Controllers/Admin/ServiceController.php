<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ServiceController extends Controller
{
    public function index(){
        return view('admin.pages.services');
    }

    public function show(){
        $reviews = Service::orderBy('id', 'desc')->where('domain_id','=',$this->getDomainActive())->get();

        return response()->json($reviews);
    }


    public function delete(Request $request){
        if ($request->isMethod('post')){
            if (Auth::check()){

                if (!empty($request->id)){
                    $delete = Service::where('id','=', $request->id)->first()->delete();

                    if ($delete) return response()->json([
                        'error' => 0,
                        'message' => 'Successfully deleted!'
                    ]);else return response()->json([
                        'error' => 1,
                        'message' => 'Oops! Something went wrong! Try again!'
                    ]);
                }

            }else return response()->json([
                'error' => 2,
                'link' => '/login'
            ]);
        }
    }

    public function update(Request $request){
        if ($request->isMethod('post')){
            if (Auth::check()){
                $errors_m = '';
                $v = Validator::make($request->all(), [
                    'label' => ['required', 'string','max:255'],
                    'link' => ['required', 'string','max:255'],
                    'description' => ['required', 'string','max:255'],
                ]);

                if ($v->fails())
                {
                    $errors = $v->errors();
                    foreach ($errors->all() as $error){
                        $errors_m.= $error;
                    }
                    return response()
                        ->json([
                            'message' => $errors_m,
                            'error' => 1,
                        ], 200);
                }

                $store = Service::where('id','=', $request->id)->first()->update([
                    'label' => htmlspecialchars(trim($request->label)),
                    'description' => htmlspecialchars(trim($request->description)),
                    'link' => trim($request->link),
                    'img' => trim($request->img),
                    'domain_id' => $this->getDomainActive(),
                    'sortorder' => trim($request->sortorder),
                    'updated_at' => Carbon::now()
                ]);


                if ($store) return response()->json([
                    'error' => 0,
                    'service' => Service::where('id','=', $request->id)->first(),
                    'message' => 'Successfully updated!'
                ]);else return response()->json([
                    'error' => 1,
                    'message' => 'Oops! Something went wrong! Try again!'
                ]);

            }else return response()->json([
                'error' => 2,
                'link' => '/login'
            ]);
        }
    }

    public function store(Request $request){
        if ($request->isMethod('post')){
            if (Auth::check()){
                $errors_m = '';
                $v = Validator::make($request->all(), [
                    'label' => ['required', 'string','max:255'],
                    'link' => ['required', 'string','max:255'],
                    'description' => ['required', 'string','max:255'],
                ]);

                if ($v->fails())
                {
                    $errors = $v->errors();
                    foreach ($errors->all() as $error){
                        $errors_m.= $error;
                    }
                    return response()
                        ->json([
                            'message' => $errors_m,
                            'error' => 1,
                        ], 200);
                }

                $store = Service::create([
                    'label' => htmlspecialchars(trim($request->label)),
                    'description' => htmlspecialchars(trim($request->description)),
                    'link' => trim($request->link),
                    'img' => trim($request->img),
                    'sortorder' => trim($request->sortorder),
                    'domain_id' => $this->getDomainActive(),
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);


                if ($store) return response()->json([
                    'error' => 0,
                    'service' => Service::where('id','=', $store->id)->first(),
                    'message' => 'Successfully added!'
                ]);else return response()->json([
                    'error' => 1,
                    'message' => 'Oops! Something went wrong! Try again!'
                ]);

            }else return response()->json([
                'error' => 2,
                'link' => '/login'
            ]);
        }
    }
}
