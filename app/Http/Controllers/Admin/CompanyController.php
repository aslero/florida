<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Category;
use App\Model\City;
use App\Model\Company;
use App\Model\CompanySocial;
use App\Model\Price;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class CompanyController extends Controller
{
    public function index(){
        return view('admin.pages.companies');
    }

    public function show(){
        $companies = DB::table('companies')
                        ->leftJoin('categories','categories.id','=','companies.category_id')
                        ->leftJoin('users','users.id','=','companies.user_id')
                        ->leftJoin('prices','prices.id','=','companies.price_id')
                        ->select('categories.title as cat','users.name as login','users.fullname','companies.*','users.id as user_id','prices.title as tariff')
                        ->where('companies.domain_id','=',$this->getDomainActive())
                        ->orderBy('companies.id','desc')->get();

        $categories = Category::where('published','=', 1)->get();
        $cities = City::where('published','=', 1)->get();
        $users = User::where('domain_id','=',$this->getDomainActive())->get();

        foreach ($companies as $company) {
            $socials = CompanySocial::where('company_id','=', $company->id)->first();
            if ($socials) $company->facebook = $socials->facebook;else $company->facebook = '';
            if ($socials) $company->google = $socials->google;else $company->google = '';
            if ($socials) $company->linkedin = $socials->linkedin;else $company->linkedin = '';
            if ($socials) $company->instagram = $socials->instagram;else $company->instagram = '';
            if ($socials) $company->twitter = $socials->twitter;else $company->twitter = '';
            if ($socials) $company->youtube = $socials->youtube;else $company->youtube = '';
        }

        return response()->json([
            'companies' => $companies,
            'categories' => $categories,
            'users' => $users,
            'cities' => $cities,
        ]);
    }

    public function filter(Request $request){
        if ($request->isMethod('GET')) {

            $companies = Company::with('user');

            if ($request->has('name') && !empty($request->name)){
                $companies = $companies->where('name','like', '%'.$request->name. '%');
            }

            if ($request->has('email') && !empty($request->email)){
                $companies = $companies->where('email','like', '%'.$request->email. '%');
            }

            $companies = $companies->where('domain_id','=',$this->getDomainActive());
            $companies = $companies->orderBy('id','desc')
                ->get();


            foreach ($companies as $company) {
                $tariff = Price::where('id','=', $company->price_id)->first();
                $company->login = $company->user->name;
                $company->fullname = $company->user->fullname;
                $company->user_id = $company->user->id;
                $company->cat = $company->category->title;
                $company->tariff = $tariff->title;

                $socials = CompanySocial::where('company_id','=', $company->id)->first();
                if ($socials) $company->facebook = $company->facebook;else $company->facebook = '';
                if ($socials) $company->google = $socials->google;else $company->google = '';
                if ($socials) $company->linkedin = $socials->linkedin;else $company->linkedin = '';
                if ($socials) $company->instagram = $socials->instagram;else $company->instagram = '';
                if ($socials) $company->twitter = $socials->twitter;else $company->twitter = '';
                if ($socials) $company->youtube = $socials->youtube;else $company->youtube = '';
            }

            return response()->json($companies);
        }
    }

    public function store(Request $request)
    {
        if ($request->isMethod('post')) {
            if (Auth::check()) {
                $errors_m = '';
                $v = Validator::make($request->all(), [
                    'name' => ['required', 'string', 'max:255'],
                    'address' => ['required', 'string', 'max:255'],
                    'zip' => ['required', 'string', 'max:255'],
                    'city_id' => ['required'],
                    'email' => ['required', 'string', 'max:255','email'],
                    'phone' => ['required', 'string', 'max:255'],
                    'time_am' => ['required'],
                    'time_pm' => ['required'],
                    'website' => ['required', 'string','max:255'],
                ]);

                if ($v->fails())
                {
                    $errors = $v->errors();
                    foreach ($errors->all() as $error){
                        $errors_m.= $error;
                    }
                    return response()
                        ->json([
                            'message' => $errors_m,
                            'error' => 1,
                        ], 200);
                }
                if ($request->hasFile('upload')){
                    $image = $request->file('upload');

                    if (empty($image->getClientOriginalExtension())) $ext = 'jpg'; else $ext = $image->getClientOriginalExtension();

                    $fileName = trim(strtolower($request->name)).'-'.trim($request->name).'-'.Str::slug(Carbon::now()->format('Y-m-d'),'-').'.'.$ext;
                    if (!empty($fileName)){
                        Storage::disk('public')->put('uploads/companies/'.$ext.'/'.$fileName, file_get_contents($image));
                    }
                    $image  = 'uploads/companies/'.$ext.'/'.$fileName;
                }else{
                    $image = '';
                }
                $store = Company::create([
                    'user_id' => $request->user_id,
                    'name' => htmlspecialchars(trim($request->name)),
                    'category_id' => $request->category_id,
                    'city_id' => $request->city_id,
                    'address' => trim($request->address),
                    'zip' => trim($request->zip),
                    'email' => trim($request->email),
                    'phone' => trim($request->phone),
                    'time_am' => trim($request->time_am),
                    'time_pm' => trim($request->time_pm),
                    'website' => trim($request->website),
                    'image' => $image,
                    'domain_id' => $this->getDomainActive(),
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);

                if ($store) {
                    $social = CompanySocial::create([
                        'company_id' => $store->id,
                        'facebook' => $request->facebook,
                        'google' => $request->google,
                        'linkedin' => $request->linkedin,
                        'instagram' => $request->instagram,
                        'twitter' => $request->twitter,
                        'youtube' => $request->youtube,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now()
                    ]);
                    $company = DB::table('companies')
                        ->leftJoin('categories','categories.id','=','companies.category_id')
                        ->leftJoin('users','users.id','=','companies.user_id')
                        ->select('categories.title as cat','users.name as login','users.fullname','companies.*','users.id as user_id')
                        ->where('companies.id','=', $store->id)
                        ->where('companies.domain_id','=',$this->getDomainActive())
                        ->orderBy('companies.id','desc')->first();

                    $socials = CompanySocial::where('company_id','=', $company->id)->first();
                    if ($socials) $company->facebook = $socials->facebook;else $company->facebook = '';
                    if ($socials) $company->google = $socials->google;else $company->google = '';
                    if ($socials) $company->linkedin = $socials->linkedin;else $company->linkedin = '';
                    if ($socials) $company->instagram = $socials->instagram;else $company->instagram = '';
                    if ($socials) $company->twitter = $socials->twitter;else $company->twitter = '';
                    if ($socials) $company->youtube = $socials->youtube;else $company->youtube = '';

                    return response()->json([
                        'error' => 0,
                        'company' => $company
                    ]);
                } else return response()->json([
                    'error' => 1,
                    'message' => 'Oops! Something went wrong! Try again!'
                ]);

            } else return response()->json([
                'error' => 2,
                'link' => '/login'
            ]);
        }
    }

    public function update(Request $request)
    {
        if ($request->isMethod('post')) {
            if (Auth::check()) {
                $errors_m = '';
                $v = Validator::make($request->all(), [
                    'name' => ['required', 'string', 'max:255'],
                    'address' => ['required', 'string', 'max:255'],
                    'zip' => ['required', 'string', 'max:255'],
                    'city_id' => ['required'],
                    'email' => ['required', 'string', 'max:255','email'],
                    'phone' => ['required', 'string', 'max:255'],
                    'time_am' => ['required'],
                    'time_pm' => ['required'],
                    'website' => ['required', 'string','max:255'],
                ]);

                if ($v->fails())
                {
                    $errors = $v->errors();
                    foreach ($errors->all() as $error){
                        $errors_m.= $error;
                    }
                    return response()
                        ->json([
                            'message' => $errors_m,
                            'error' => 1,
                        ], 200);
                }
                if ($request->hasFile('upload')){
                    $image = $request->file('upload');

                    if (empty($image->getClientOriginalExtension())) $ext = 'jpg'; else $ext = $image->getClientOriginalExtension();

                    $fileName = trim(strtolower($request->name)).'-'.trim($request->name).'-'.Str::slug(Carbon::now()->format('Y-m-d'),'-').'.'.$ext;
                    if (!empty($fileName)){
                        if (!empty($request->image)){
                            Storage::disk('public')->delete('/storage/'.$request->image);
                        }
                        Storage::disk('public')->put('uploads/companies/'.$ext.'/'.$fileName, file_get_contents($image));
                    }
                    $image  = 'uploads/companies/'.$ext.'/'.$fileName;
                }else{
                    $image = $request->image;
                }

                $store = Company::where('id','=',$request->id)->first()->update([
                    'user_id' => $request->user_id,
                    'name' => htmlspecialchars(trim($request->name)),
                    'category_id' => $request->category_id,
                    'city_id' => $request->city_id,
                    'address' => trim($request->address),
                    'zip' => trim($request->zip),
                    'email' => trim($request->email),
                    'phone' => trim($request->phone),
                    'time_am' => trim($request->time_am),
                    'time_pm' => trim($request->time_pm),
                    'website' => trim($request->website),
                    'image' => $image,
                    'domain_id' => $this->getDomainActive(),
                    'updated_at' => Carbon::now()
                ]);

                if ($store) {
                    $socialBase = CompanySocial::where('company_id','=',$request->id)->first();
                    if ($socialBase){
                        $socialBase->update([
                            'facebook' => $request->facebook,
                            'google' => $request->google,
                            'linkedin' => $request->linkedin,
                            'instagram' => $request->instagram,
                            'twitter' => $request->twitter,
                            'youtube' => $request->youtube,
                            'updated_at' => Carbon::now()
                        ]);
                    }else{
                        CompanySocial::create([
                            'company_id' => $request->id,
                            'facebook' => $request->facebook,
                            'google' => $request->google,
                            'linkedin' => $request->linkedin,
                            'instagram' => $request->instagram,
                            'twitter' => $request->twitter,
                            'youtube' => $request->youtube,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now()
                        ]);
                    }
                    $company = DB::table('companies')
                        ->leftJoin('categories','categories.id','=','companies.category_id')
                        ->leftJoin('users','users.id','=','companies.user_id')
                        ->select('categories.title as cat','users.name as login','users.fullname','companies.*','users.id as user_id')
                        ->where('companies.id','=', $request->id)
                        ->where('companies.domain_id','=',$this->getDomainActive())
                        ->orderBy('companies.id','desc')->first();

                    $socials = CompanySocial::where('company_id','=', $company->id)->first();
                    if ($socials) $company->facebook = $socials->facebook;else $company->facebook = '';
                    if ($socials) $company->google = $socials->google;else $company->google = '';
                    if ($socials) $company->linkedin = $socials->linkedin;else $company->linkedin = '';
                    if ($socials) $company->instagram = $socials->instagram;else $company->instagram = '';
                    if ($socials) $company->twitter = $socials->twitter;else $company->twitter = '';
                    if ($socials) $company->youtube = $socials->youtube;else $company->youtube = '';

                    return response()->json([
                        'error' => 0,
                        'company' => $company
                    ]);
                } else return response()->json([
                    'error' => 1,
                    'message' => 'Oops! Something went wrong! Try again!'
                ]);

            } else return response()->json([
                'error' => 2,
                'link' => '/login'
            ]);
        }
    }

    public function delete(Request $request){
        if ($request->isMethod('post')){
            if (Auth::check()){

                if (!empty($request->id)){
                    $delete = Company::where('id','=', $request->id)->first()->delete();

                    if ($delete) return response()->json([
                        'error' => 0,
                        'message' => 'Successfully deleted!'
                    ]);else return response()->json([
                        'error' => 1,
                        'message' => 'Oops! Something went wrong! Try again!'
                    ]);
                }

            }else return response()->json([
                'error' => 2,
                'link' => '/login'
            ]);
        }
    }

}
