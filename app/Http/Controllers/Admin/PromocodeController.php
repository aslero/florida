<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Promocode;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class PromocodeController extends Controller
{
    public function index(){
        return view('admin.pages.promocodes');
    }

    public function show(){
        $reviews = Promocode::orderBy('id', 'desc')->where('domain_id','=',$this->getDomainActive())->get();

        return response()->json($reviews);
    }

    public function search(Request $request){
        if (isset($request->search)){
            $reviews = Promocode::where('first_name','like', '%'.$request->search. '%')
                ->orWhere('last_name','like', '%'.$request->search. '%')
                ->orWhere('code','like', '%'.$request->search. '%')
                ->orWhere('title','like', '%'.$request->search. '%')
                ->where('domain_id','=',$this->getDomainActive())
                ->orderBy('reviews.id', 'desc')
                ->get();
        }else{
            $reviews = Promocode::orderBy('id', 'desc')->get();
        }

        return response()->json($reviews);
    }


    public function delete(Request $request){
        if ($request->isMethod('post')){
            if (Auth::check()){

                if (!empty($request->id)){
                    $delete = Promocode::where('id','=', $request->id)->first()->delete();

                    if ($delete) return response()->json([
                        'error' => 0,
                        'message' => 'Successfully deleted!'
                    ]);else return response()->json([
                        'error' => 1,
                        'message' => 'Oops! Something went wrong! Try again!'
                    ]);
                }

            }else return response()->json([
                'error' => 2,
                'link' => '/login'
            ]);
        }
    }

    public function update(Request $request){
        if ($request->isMethod('post')){
            if (Auth::check()){
                $errors_m = '';
                $v = Validator::make($request->all(), [
                    'first_name' => ['required', 'string'],
                    'code' => ['required', 'string'],
                    'phone' => ['required', 'string'],
                    'percent' => ['required'],
                ]);

                if ($v->fails())
                {
                    $errors = $v->errors();
                    foreach ($errors->all() as $error){
                        $errors_m.= $error;
                    }
                    return response()
                        ->json([
                            'message' => $errors_m,
                            'error' => 1,
                        ], 200);
                }

                $store = Promocode::where('id','=', $request->id)->first()->update([
                    'title' => htmlspecialchars(trim($request->title)),
                    'last_name' => htmlspecialchars(trim($request->last_name)),
                    'first_name' => htmlspecialchars(trim($request->first_name)),
                    'company' => htmlspecialchars(trim($request->company)),
                    'phone' => trim($request->phone),
                    'code' => trim($request->code),
                    'percent' => trim($request->percent),
                    'comment' => trim($request->comment),
                    'updated_at' => Carbon::now()
                ]);


                if ($store) return response()->json([
                    'error' => 0,
                    'promo' => Promocode::where('id','=', $request->id)->first(),
                    'message' => 'Successfully updated!'
                ]);else return response()->json([
                    'error' => 1,
                    'message' => 'Oops! Something went wrong! Try again!'
                ]);

            }else return response()->json([
                'error' => 2,
                'link' => '/login'
            ]);
        }
    }

    public function store(Request $request){
        if ($request->isMethod('post')){
            if (Auth::check()){
                $errors_m = '';
                $v = Validator::make($request->all(), [
                    'first_name' => ['required', 'string'],
                    'code' => ['required', 'string'],
                    'phone' => ['required', 'string'],
                    'percent' => ['required'],
                ]);

                if ($v->fails())
                {
                    $errors = $v->errors();
                    foreach ($errors->all() as $error){
                        $errors_m.= $error;
                    }
                    return response()
                        ->json([
                            'message' => $errors_m,
                            'error' => 1,
                        ], 200);
                }

                $store = Promocode::create([
                    'title' => htmlspecialchars(trim($request->title)),
                    'last_name' => htmlspecialchars(trim($request->last_name)),
                    'first_name' => htmlspecialchars(trim($request->first_name)),
                    'company' => htmlspecialchars(trim($request->company)),
                    'phone' => trim($request->phone),
                    'code' => trim($request->code),
                    'percent' => trim($request->percent),
                    'comment' => trim($request->comment),
                    'status' => 1,
                    'user_id' => Auth::user()->id,
                    'domain_id' => $this->getDomainActive(),
                    'date_to' => Carbon::now()->addDays(7),
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ]);


                if ($store) return response()->json([
                    'error' => 0,
                    'promo' => $store,
                    'message' => 'Successfully added!'
                ]);else return response()->json([
                    'error' => 1,
                    'message' => 'Oops! Something went wrong! Try again!'
                ]);

            }else return response()->json([
                'error' => 2,
                'link' => '/login'
            ]);
        }
    }
}
