<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Payment;
use App\Model\Price;
use App\Model\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UsersController extends Controller
{
    public function index(){
        return view('admin.pages.users');
    }

    public function payments($id){
        return view('admin.pages.user-payments',[
            'id' => $id
        ]);
    }

    public function userPayments(Request $request){
        $payments = Payment::where('user_id','=', $request->id)->get();

        return response()->json($payments);
    }

    public function show(){
        $roles = Role::all();
        $users = User::where('domain_id','=',$this->getDomainActive())->get();

        foreach ($users as $user) {
            $payment = Payment::where('user_id','=', $user->id)->where('date_to','>', Carbon::now())->first();
            if ($payment){
                $tariff = Price::where('id','=', $payment->tariff)->first();
                $user->tariff = $tariff->title;
                $user->date_to = $payment->date_to;
            }else{
                $user->tariff = null;
                $user->date_to = null;
            }

        }

        return response()->json([
            'roles' => $roles,
            'users' => $users
        ]);
    }

    public function store(Request $request)
    {
        if ($request->isMethod('post')) {
            if (Auth::check()) {

                $errors_m = '';


                $v = Validator::make($request->all(), [
                    'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                    'fullname' => ['required', 'string', 'max:255'],
                    'phone' => ['required', 'string', 'max:255'],
                    'name' => ['required', 'string', 'max:255'],
                    'password' => ['required', 'string', 'min:6'],
                ]);

                if ($v->fails())
                {
                    $errors = $v->errors();
                    foreach ($errors->all() as $error){
                        $errors_m.= $error;
                    }
                    return response()
                        ->json([
                            'message' => $errors_m,
                            'error' => 1,
                        ], 200);
                }

                $store = User::create([
                    'name' => htmlspecialchars(trim($request->name)),
                    'fullname' => htmlspecialchars(trim($request->fullname)),
                    'email' => trim($request->email),
                    'phone' => trim($request->phone),
                    'pass' => trim($request->password),
                    'password' => Hash::make($request->password),
                    'role_id' => $request->role_id,
                    'domain_id' => $this->getDomainActive(),
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);

                if ($store) {
                    return response()->json([
                        'error' => 0,
                        'user' => $store
                    ]);
                } else return response()->json([
                    'error' => 1,
                    'message' => 'Oops! Something went wrong! Try again!'
                ]);

            } else return response()->json([
                'error' => 2,
                'link' => '/login'
            ]);
        }
    }

    public function update(Request $request)
    {
        if ($request->isMethod('post')) {
            if (Auth::check()) {
                $errors_m = '';


                $v = Validator::make($request->all(), [
                    'email' => ['required', 'string', 'email', 'max:255'],
                    'fullname' => ['required', 'string', 'max:255'],
                    'phone' => ['required', 'string', 'max:255'],
                    'name' => ['required', 'string', 'max:255'],
                    'password' => ['required', 'string', 'min:6'],
                ]);

                if ($v->fails())
                {
                    $errors = $v->errors();
                    foreach ($errors->all() as $error){
                        $errors_m.= $error;
                    }
                    return response()
                        ->json([
                            'message' => $errors_m,
                            'error' => 1,
                        ], 200);
                }


                $user = User::where('id', '=', $request->id)->first()->update([
                    'name' => htmlspecialchars(trim($request->name)),
                    'fullname' => htmlspecialchars(trim($request->fullname)),
                    'email' => trim($request->email),
                    'phone' => trim($request->phone),
                    'pass' => trim($request->password),
                    'password' => Hash::make($request->password),
                    'role_id' => $request->role_id,
                    'domain_id' => $this->getDomainActive(),
                    'updated_at' => Carbon::now()
                ]);

                if ($user) {
                    return response()->json([
                        'error' => 0,
                        'user' => User::where('id', '=',  $request->id)->first()
                    ]);
                } else return response()->json([
                    'error' => 1,
                    'message' => 'Oops! Something went wrong! Try again!'
                ]);

            } else return response()->json([
                'error' => 2,
                'link' => '/login'
            ]);
        }
    }

    public function delete(Request $request){
        if ($request->isMethod('post')){
            if (Auth::check()){

                if (!empty($request->id)){
                    $delete = User::where('id','=', $request->id)->first()->delete();

                    if ($delete) return response()->json([
                        'error' => 0,
                        'message' => 'Successfully deleted!'
                    ]);else return response()->json([
                        'error' => 1,
                        'message' => 'Oops! Something went wrong! Try again!'
                    ]);
                }

            }else return response()->json([
                'error' => 2,
                'link' => '/login'
            ]);
        }
    }
}
