<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Contact;
use App\Model\Content;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ContactController extends Controller
{
    public function index(){
        return view('admin.pages.contacts');
    }

    public function show(){
        $contacts = Contact::orderBy('id', 'asc')->first();
        return response()->json($contacts);
    }

    public function update(Request $request){
        if ($request->isMethod('post')){
            if (Auth::check()){
                $contacts = Contact::first();
                if ($contacts){
                    $update = $contacts->update([
                        'skype' => $request->skype,
                        'skype_work' => $request->skype_work,
                        'email' => $request->email,
                        'email_work' => $request->email_work,
                        'twitter' => $request->twitter,
                        'youtube' => $request->youtube,
                        'instagram' => $request->instagram,
                        'facebook' => $request->facebook,
                    ]);
                    if ($update) return response()->json([
                        'error' => 0,
                        'message' => 'Contact details updated!'
                    ]);
                }else{
                    $create = Contact::create([
                        'skype' => $request->skype,
                        'skype_work' => $request->skype_work,
                        'email' => $request->email,
                        'email_work' => $request->email_work,
                        'twitter' => $request->twitter,
                        'youtube' => $request->youtube,
                        'instagram' => $request->instagram,
                        'facebook' => $request->facebook,
                    ]);
                    if ($create) return response()->json([
                        'error' => 0,
                        'message' => 'Contact details updated!'
                    ]);
                }

                return response()->json([
                    'error' => 1,
                    'message' => 'Oops! Something went wrong! Try again!'
                ]);
            }
        }else return response()->json([
            'error' => 2,
            'link' => '/login'
        ]);
    }
}
