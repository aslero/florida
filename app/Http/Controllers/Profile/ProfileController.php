<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Model\Payment;
use App\Model\Price;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
     public function index()
    {
        $payments = Payment::where('user_id','=', Auth::user()->id)->orderBy('created_at','desc')->take(6)->get();

        $latestPayment = Payment::where('user_id','=', Auth::user()->id)->latest()->first();
        $tariff = Price::where('id','=',$latestPayment->tariff)->first();

    	return view('pages.profile.index',[
    	    'payments' => $payments,
    	    'latestPayment' => $latestPayment,
    	    'tariff' => $tariff,
        ]);
    }

    public function update(Request $request){
         if ($request->isMethod('POST')){

             $user = User::where('id','=', Auth::user()->id)->first();

             $user->update([
                 'fullname' => htmlspecialchars(trim($request->fullname)),
                 'phone' => trim($request->phone),
                 'pass' => trim($request->password),
                 'password' => Hash::make($request->password)
             ]);

             return redirect()->route('profile');
         }else{
             return redirect()->route('home');
         }
    }

    public function logout(){
        Auth::logout();
        return redirect('/');
    }
}
