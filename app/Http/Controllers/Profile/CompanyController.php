<?php

namespace App\Http\Controllers\Profile;


use App\Http\Controllers\Controller;
use App\Model\Category;
use App\Model\City;
use App\Model\Company;
use App\Model\CompanySocial;
use App\Model\Payment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class CompanyController extends Controller
{
    public function edit($id)
    {
        $cities = City::where('published','=', 1)->get();
        $company = Company::where('id','=', $id)->where('user_id','=', Auth::user()->id)->first();
        $socials = CompanySocial::where('company_id','=', $id)->first();
        if ($socials) $facebook = $socials->facebook;else $facebook = '';
        if ($socials) $google = $socials->google;else $google = '';
        if ($socials) $linkedin = $socials->linkedin;else $linkedin = '';
        if ($socials) $instagram = $socials->instagram;else $instagram = '';
        if ($socials) $twitter = $socials->twitter;else $twitter = '';
        if ($socials) $youtube = $socials->youtube;else $youtube = '';

    	return view('pages.profile.edit-company',[
    	    'company' => $company,
    	    'facebook' => $facebook,
    	    'google' => $google,
    	    'linkedin' => $linkedin,
    	    'instagram' => $instagram,
    	    'twitter' => $twitter,
    	    'youtube' => $youtube,
            'categories' => Category::where('published','=', 1)->get(),
            'cities' => $cities
        ]);
    }
    
    public function create()
    {
        $cities = City::where('published','=', 1)->get();
        $categories = Category::where('published','=', 1)->get();
    	return view('pages.profile.add-company',['categories' => $categories,'cities' => $cities]);
    }

    public function store(Request $request){

        $v = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255','unique:companies'],
            'address' => ['required', 'string', 'max:255'],
            'zip' => ['required', 'string', 'max:255'],
            'city_id' => ['required'],
            'email' => ['required', 'string', 'max:255','email'],
            'phone' => ['required', 'string', 'max:255'],
            'time_am' => ['required'],
            'time_pm' => ['required'],
            'website' => ['required', 'string','max:255'],
        ]);

        if ($v->fails())
        {
            return redirect()->back()->withErrors($v->errors())->withInput();
        }

        $payment = Payment::where('user_id','=', Auth::user()->id)->whereBetween('count_company', [1, 10])->first();
        if ($payment) $price = $payment->tariff;else $price = 0;

        if ($request->hasFile('image')){
            $image = $request->file('image');

            if (empty($image->getClientOriginalExtension())) $ext = 'jpg'; else $ext = $image->getClientOriginalExtension();

            $fileName = trim(strtolower(Str::slug($request->name,'-'))).'-'.Auth::user()->id.'-'.Str::slug(Carbon::now()->format('Y-m-d'),'-').'.'.$ext;
            if (!empty($fileName)){
                Storage::disk('public')->put('uploads/companies/'.$ext.'/'.$fileName, file_get_contents($image));
            }
            $image  = 'uploads/companies/'.$ext.'/'.$fileName;
        }else{
            $image = '';
        }

        $store = Company::create([
            'user_id' => Auth::user()->id,
            'name' => htmlspecialchars(trim($request->name)),
            'category_id' => $request->category_id,
            'city_id' => $request->city_id,
            'address' => trim($request->address),
            'zip' => trim($request->zip),
            'email' => trim($request->email),
            'phone' => trim($request->phone),
            'time_am' => trim($request->time_am),
            'time_pm' => trim($request->time_pm),
            'website' => trim($request->website),
            'image' => $image,
            'socials' => json_encode($request->social),
            'price_id' => $price,
            'domain_id' => $this->getDomainActive(),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        if ($store){
            $social = CompanySocial::create([
                'company_id' => $store->id,
                'facebook' => $request->facebook,
                'google' => $request->google,
                'linkedin' => $request->linkedin,
                'instagram' => $request->instagram,
                'twitter' => $request->twitter,
                'youtube' => $request->youtube,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);

            if ($price > 0){
                $payment->update([
                    'count_company' => $payment->count_company - 1
                ]);
            }
            return redirect()->route('business');

        }else return redirect()->back();
    }

    public function update(Request $request, $id){
        $updatePrice = 0; // Флаг, если не было тарифа, чтобы уменьшить счетчик count_company в Payment модели

        $v = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'address' => ['required', 'string', 'max:255'],
            'zip' => ['required', 'string', 'max:255'],
            'city_id' => ['required'],
            'email' => ['required', 'string', 'max:255','email'],
            'phone' => ['required', 'string', 'max:255'],
            'time_am' => ['required'],
            'time_pm' => ['required'],
            'website' => ['required', 'string','max:255'],
        ]);

        if ($v->fails())
        {
            return redirect()->back()->withErrors($v->errors())->withInput();
        }

        $company = Company::where('id','=',$id)->first();


        if ($company->price_id == 0){
            $payment = Payment::where('user_id','=', Auth::user()->id)->whereBetween('count_company', [1, 10])->first();
            if ($payment){
                $price = $payment->tariff;
                $updatePrice = 1;
            }else $price = 0;
        }else{
            $price = $company->price_id;
            $updatePrice = 0;
        }


        if ($request->hasFile('image')){
            $image = $request->file('image');

            if (empty($image->getClientOriginalExtension())) $ext = 'jpg'; else $ext = $image->getClientOriginalExtension();

            $fileName = trim(strtolower(Str::slug($request->name,'-'))).'-'.Auth::user()->id.'-'.Str::slug(Carbon::now()->format('Y-m-d'),'-').'.'.$ext;
            if (!empty($fileName)){
                Storage::disk('public')->delete('/storage/'.$company->image);
                Storage::disk('public')->put('uploads/companies/'.$ext.'/'.$fileName, file_get_contents($image));
            }
            $image  = 'uploads/companies/'.$ext.'/'.$fileName;
        }else{
            $image = $company->image;
        }

        $store = $company->update([
            'name' => htmlspecialchars(trim($request->name)),
            'category_id' => $request->category_id,
            'city_id' => $request->city_id,
            'address' => trim($request->address),
            'zip' => trim($request->zip),
            'email' => trim($request->email),
            'phone' => trim($request->phone),
            'time_am' => trim($request->time_am),
            'time_pm' => trim($request->time_pm),
            'website' => trim($request->website),
            'image' => $image,
            'price_id' => $price,
            'updated_at' => Carbon::now()
        ]);

        if ($store){
            $socialBase = CompanySocial::where('company_id','=',$id)->first();
            if ($socialBase){
                $socialBase->update([
                    'facebook' => $request->facebook,
                    'google' => $request->google,
                    'linkedin' => $request->linkedin,
                    'instagram' => $request->instagram,
                    'twitter' => $request->twitter,
                    'youtube' => $request->youtube,
                    'updated_at' => Carbon::now()
                ]);
            }else{
                CompanySocial::create([
                    'company_id' => $id,
                    'facebook' => $request->facebook,
                    'google' => $request->google,
                    'linkedin' => $request->linkedin,
                    'instagram' => $request->instagram,
                    'twitter' => $request->twitter,
                    'youtube' => $request->youtube,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);
            }
            if ($updatePrice == 1){
                $payment->update([
                    'count_company' => $payment->count_company - 1
                ]);
            }
            return redirect()->route('business');
        }else return redirect()-route('edit-company', $id);
    }

    public function delete(Request $request){
        if ($request->isMethod('POST')){
            if ($request->id && $request->id > 0){
                $delete = Company::where('id','=', $request->id)->where('user_id','=', Auth::user()->id)->first()->delete();

                if ($delete){
                    return response()->json([
                        'error' => 0,
                        'message' => Lang::get('companies.delete_success')
                    ]);
                }else{
                    return response()->json([
                        'error' => 1,
                        'message' => Lang::get('companies.delete_error')
                    ]);
                }
            }else{
                return response()->json([
                    'error' => 1,
                    'message' => Lang::get('companies.delete_empty')
                ]);
            }
        }else{
            return response()->json([
                'error' => 2,
                'message' => '',
                'link' => '/profile/business'
            ]);
        }
    }
}
