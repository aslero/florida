<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Model\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BusinessController extends Controller
{
    public function index()
    {
        $companies = Company::where('user_id','=', Auth::user()->id)->orderBy('created_at','desc')->paginate(15);

    	return view('pages.profile.business',[
    	    'companies' => $companies
        ]);
    }
}
