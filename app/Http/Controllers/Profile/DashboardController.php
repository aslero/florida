<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Model\Company;
use App\Model\CompanyStat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function index()
    {
        $companies = Company::where('user_id','=', Auth::user()->id)->orderBy('created_at','desc')->paginate(15);

        foreach ($companies as $company){
            $company->sites = CompanyStat::where('company_id','=', $company->id)->where('published','=', 1)->count();
            $company->users = CompanyStat::where('company_id','=', $company->id)->where('published','=', 1)->sum('user');
            $company->on = CompanyStat::where('company_id','=', $company->id)->where('status','=', 1)->count();
            $company->off = CompanyStat::where('company_id','=', $company->id)->where('status','=', 0)->count();
        }

        return view('pages.profile.dashboard',[
            'companies' => $companies
        ]);
    }
}
