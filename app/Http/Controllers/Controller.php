<?php

namespace App\Http\Controllers;

use App\Model\Domain;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getDomainActive(){

        if (!Cache::get('domain')){
            $domain = Domain::where('domain','=',env('DOMAIN'))->first();

            $expiresAt = Carbon::now()->addDays(365);
            Cache::put('domain', $domain->id, $expiresAt);
            Cache::put('domain_name', $domain->domain, $expiresAt);
            Cache::put('domain_title', $domain->title, $expiresAt);
            return Cache::get('domain');
        }else{
            return Cache::get('domain');
        }
    }

    public function setDomainActive($id){
        $domain = Domain::where('id','=',$id)->first();
        $expiresAt = Carbon::now()->addDays(365);
        Cache::put('domain', $domain->id, $expiresAt);
        Cache::put('domain_name', $domain->domain, $expiresAt);
        Cache::put('domain_title', $domain->title, $expiresAt);
    }
}
