<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Mail\EmailContact;
use App\Model\Content;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class ContactsController extends Controller
{
     public function index()
    {
        $content = Content::where('slug','=','contacts')->first();
    	return view('pages.contacts',[
    	    'content' => $content
        ]);
    }

    public function send(Request $request){

        $errors_m = '';


        $v = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'max:255','email'],
            'message' => ['required'],
        ]);

        if ($v->fails())
        {
            $errors = $v->errors();
            foreach ($errors->all() as $error){
                $errors_m.= $error;
            }
            return response()
                ->json([
                    'message' => $errors_m,
                    'error' => 1,
                ], 200);
        }

        Mail::to($request->email)->send(new EmailContact($request->name,$request->email,$request->message));

        if (Mail::failures()){
            return response()->json([
                'error' => 1,
                'message' => Lang::get('contacts.send_error')
            ]);
        }

        return response()->json([
            'error' => 0,
            'message' => Lang::get('contacts.send_success')
        ]);
    }
}
