<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Model\Content;
use App\Model\Service;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
     public function index()
    {
        $content = Content::where('slug','=','services')->first();
        $services = Service::orderBy('sortorder','asc')->get();
    	return view('pages.service',[
            'content' => $content,
            'services' => $services,
        ]);
    }
}
