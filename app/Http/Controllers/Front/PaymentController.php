<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Model\Payment;
use App\Model\Price;
use App\Model\Promocode;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Stripe\Error\Card;
use Cartalyst\Stripe\Stripe;

class PaymentController extends Controller
{
    public function index(Request $request)
    {
        if ($request->isMethod('GET')){
            if ($request->id == 0 && $request->price == 0){
                $payment = Payment::create([
                    'user_id' => Auth::user()->id,
                    'cost' =>  0,
                    'tariff' =>  0,
                    'chargeid' =>  '',
                    'count_company' => 1,
                    'status' => 1,
                    'domain_id' => $this->getDomainActive(),
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);
                return redirect()->route('profile');
            }else{
                if (!empty($request->price) && !empty($request->id)){
                    $promocode = 0;
                    if (!empty($request->promocode)){
                        $promo = Promocode::where('id','=',$request->promocode)->first();
                        if ($promo && $promo->status == 1){
                            $promocode = $request->promocode;
                        }
                    }
                    return view('pages.payments.tariffs',[
                        'price' => trim($request->price),
                        'id' => trim($request->id),
                        'promocode' => $promocode,
                    ]);
                }
            }

        }
        return redirect()->route('price');

    }
    public function payment(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'card_no' => 'required',
            'ccExpiryMonth' => 'required',
            'ccExpiryYear' => 'required',
            'cvvNumber' => 'required',
            'amount' => 'required',
        ]);


        if ($validator->passes()) {
            $stripe = Stripe::make(env('STRIPE_SECRET'));
            try {
                $token = $stripe->tokens()->create([
                    'card' => [
                        'number' => $request->get('card_no'),
                        'exp_month' => $request->get('ccExpiryMonth'),
                        'exp_year' => $request->get('ccExpiryYear'),
                        'cvc' => $request->get('cvvNumber'),
                    ],
                ]);
                if (!isset($token['id'])) {
                    return redirect()->route('home');
                }
                $charge = $stripe->charges()->create([
                    'card' => $token['id'],
                    'currency' => 'USD',
                    'amount' => trim($request->amount),
                    'description' => 'user - '.Auth::user()->fullname.',email - '.Auth::user()->email,
                ]);

                if($charge['status'] == 'succeeded') {
                    $tariff = Price::where('id','=', $request->id)->first();
                    if ($tariff->days == 0) $date_to = Carbon::now()->addDays(365)->format('Y-m-d');else $date_to = Carbon::now()->addDays($tariff->days)->format('Y-m-d');

                    $promocode = 0;
                    if (!empty($request->promocode)){
                        $promo = Promocode::where('id','=',$request->promocode)->first();
                        if ($promo && $promo->status == 1){
                            $promocode = $request->promocode;
                        }
                    }

                    $payment = Payment::where('user_id','=', Auth::user()->id)
                                           ->where('tariff','=',trim($request->id))
                                           ->where('status','=',0)
                                           ->where('cost','=',0)->first();
                    if ($payment){
                        $payment->update([
                            'cost' =>  trim($request->amount),
                            'chargeid' =>  $charge['id'],
                            'status' => 1,
                            'promocode_id' => $promocode,
                            'updated_at' => Carbon::now()
                        ]);
                    }else{
                        $payment = Payment::create([
                            'user_id' => Auth::user()->id,
                            'cost' =>  trim($request->amount),
                            'tariff' =>  trim($request->id),
                            'chargeid' =>  $charge['id'],
                            'count_company' => 10,
                            'status' => 1,
                            'date_to' => $date_to,
                            'promocode_id' => $promocode,
                            'domain_id' => $this->getDomainActive(),
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now()
                        ]);
                    }



                    if ($payment && $promocode > 0){
                        Promocode::where('id','=', $promocode)->first()->update([
                            'status' => 2
                        ]);
                    }

                   // echo "<pre>";
                   // print_r($charge);
                   // exit();
                    //return redirect()->route('payments.success');
                    return view('pages.payments.success',[
                        'payment' => $payment
                    ]);

                } else {
                    Session::put('error','Money not add in wallet!!');
                    return redirect()->route('payments');
                }
            } catch (Exception $e) {
                Session::put('error',$e->getMessage());
                return redirect()->route('payments');
            } catch(\Cartalyst\Stripe\Exception\CardErrorException $e) {
                Session::put('error',$e->getMessage());
                return redirect()->route('payments');
            } catch(\Cartalyst\Stripe\Exception\MissingParameterException $e) {
                Session::put('error',$e->getMessage());
                return redirect()->route('payments');
            }
        }
    }
}
