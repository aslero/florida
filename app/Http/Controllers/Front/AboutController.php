<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Model\Content;
use Illuminate\Http\Request;

class AboutController extends Controller
{
    public function index()
    {
        $contact = Content::where('slug','=','contacts')->first();
        $content = Content::where('slug','=','about')->first();
        return view('pages.about',[
            'content' => $content,
            'contact' => $contact,
        ]);
    }
}
