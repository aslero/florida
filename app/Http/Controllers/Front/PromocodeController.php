<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Model\Price;
use App\Model\Promocode;
use Illuminate\Http\Request;

class PromocodeController extends Controller
{
    public function index(Request $request){
        $sale = 0;
        if ($request->isMethod('GET')){
            if (!empty($request->promo) && $request->tariff){
                $promocode = Promocode::where('code','=', trim($request->promo))->first();
                if ($promocode && $promocode->status == 1){
                    $tariff = Price::where('id','=', $request->tariff)->first();
                    if ($tariff && $tariff->price > 0){
                        $sale = round($tariff->price - ($tariff->price * ($promocode->percent / 100)),2);

                        return response()->json([
                            'error' => 0,
                            'sale' => $sale,
                            'promocode' => $promocode->id
                        ]);
                    }else{
                        return response()->json([
                            'error' => 1,
                            'message' => 'There is no tariff! Check your entries!'
                        ]);
                    }
                }else{
                    return response()->json([
                        'error' => 1,
                        'message' => 'The promotional code is inactive or has already been used!'
                    ]);
                }
            }else{
                return response()->json([
                    'error' => 1,
                    'message' => 'Check your entries!'
                ]);
            }
        }else{
            return response()->json([
                'error' => 2,
                'link' => '/'
            ]);
        }
    }
}
