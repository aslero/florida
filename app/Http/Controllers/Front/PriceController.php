<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Model\Compare;
use App\Model\Price;
use App\Model\PriceCompare;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PriceController extends Controller
{
     public function index()
    {
        $tariffs = Price::where('published','=', 1)->get();

        $compares = Compare::select('id','title')->get();

        foreach ($compares as $compare) {
            $compare->tariff = PriceCompare::where('compare_id','=',$compare->id)->get();
            foreach ($compare->tariff as $item) {
                $special = Price::where('id','=',$item->price_id)->select()->first();
                $item->special = $special->special;
            }
        }
    	return view('pages.price',[
    	    'tariffs' => $tariffs,
    	    'compares' => $compares,
        ]);
    }
}
