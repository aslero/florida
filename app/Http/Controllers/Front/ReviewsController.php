<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Model\Review;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class ReviewsController extends Controller
{
     public function index()
    {
        $reviews = Review::where('published','=', 1)->orderBy('updated_at','desc')->get();
    	return view('pages.reviews',[
    	    'reviews' => $reviews
        ]);
    }

    public function store(Request $request){

        $errors_m = '';


        $v = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'max:255','email'],
            'comment' => ['required'],
            'raiting' => ['required'],
        ]);

        if ($v->fails())
        {
            $errors = $v->errors();
            foreach ($errors->all() as $error){
                $errors_m.= $error;
            }
            return response()
                ->json([
                    'message' => $errors_m,
                    'error' => 1,
                ], 200);
        }

        $store = Review::create([
            'name' => htmlspecialchars(trim($request->name)),
            'email' => trim($request->email),
            'text' => trim($request->comment),
            'raiting' => $request->raiting,
            'ip' => $_SERVER['REMOTE_ADDR'],
            'published' => 0,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        if ($store) return response()->json([
            'error' => 0,
            'message' => Lang::get('reviews.success')
        ]);else return response()->json([
            'error' => 1,
            'message' => Lang::get('reviews.error')
        ]);
    }
}
