<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 02.03.2020
 * Time: 10:44
 */

namespace App;


class CustomRequestCaptcha
{
    public function custom()
    {
        return new \ReCaptcha\RequestMethod\CurlPost();
    }
}