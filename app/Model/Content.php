<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    protected $table = 'contents';
    protected $fillable = [
        'pagetitle','description','meta_pagetitle','meta_keywords','meta_description',
        'content','slug','img','published','hidemenu','bottom'
    ];
}
