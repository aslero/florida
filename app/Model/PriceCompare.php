<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PriceCompare extends Model
{
   protected $table = 'price_compares';

   protected $fillable = [
       'price_id','compare_id','availability','comment'
   ];
}
