<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Promocode extends Model
{
    protected $table = 'promocodes';

    protected $fillable = [
        'title','first_name','last_name','company','phone','code','percent','comment','status','user_id','date_to','domain_id'
    ];
}
