<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ServiceStat extends Model
{
    protected $table = 'service_stats';

    protected $fillable = [
        'service_id','title','value','published'
    ];

    public function servie()
    {
        return $this->belongsTo(Service::class);
    }
}
