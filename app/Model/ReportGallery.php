<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ReportGallery extends Model
{
    protected $table = 'report_galleries';

    protected $fillable = [
        'report_id','image','description','published'
    ];

    public function report()
    {
        return $this->belongsTo(Report::class);
    }
}
