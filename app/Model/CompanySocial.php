<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CompanySocial extends Model
{
    protected $table = 'company_socials';

    protected $fillable = [
        'company_id','facebook','google','linkedin','instagram','twitter','youtube'
    ];
}
