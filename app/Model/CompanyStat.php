<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CompanyStat extends Model
{
    protected $table = 'company_stats';

    protected $fillable = [
        'company_id','name','link','user','status','subscription','moderation','published'
    ];

}
