<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = 'payments';

    protected $fillable = [
        'user_id','cost','tariff','count_company','status','arhive','chargeid','date_to','domain_id','promocode_id'
    ];

    public function user() //Привязываем к модели пользователя
    {
        return $this->belongsTo(User::class,'user_id');
    }
}
