<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    protected $table = 'providers';

    protected $fillable = [
        'user_id','provider','provider_id','nickname','name','email','avatar'
    ];
}
