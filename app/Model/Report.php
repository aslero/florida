<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $table = 'reports';

    protected $fillable = [
        'label','description','sortorder','domain_id'
    ];


    public function galleries()
    {
        return $this->hasMany(ReportGallery::class,'report_id');
    }
}
