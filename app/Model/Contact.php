<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table = 'contacts';

    protected $fillable = [
      'email','email_work','skype','skype_work','twitter','facebook','instagram','youtube'
    ];
}
