<?php

namespace App\Model;


use App\User;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{

    protected $table = 'companies';

    protected $fillable = [
        'user_id','name','category_id','city_id','address','zip','email','phone','time_am','time_pm','website','image','status','moderator','price_id','domain_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function price()
    {
        return $this->belongsTo(Price::class,'price_id');
    }

    public function cities()
    {
        return $this->belongsTo(City::class,'city_id');
    }

    public function category(){
        return $this->belongsTo(Category::class,'category_id');
    }

    public function stats()
    {
        return $this->hasMany(CompanyStat::class,'company_id');
    }

    public function socials()
    {
        return $this->hasMany(CompanySocial::class,'company_id');
    }
}
