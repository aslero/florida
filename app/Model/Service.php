<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $table = 'services';

    protected $fillable = [
        'label','link','description','img','sortorder','domain_id'
    ];

    public function stats()
    {
        return $this->hasMany(ServiceStat::class,'service_id');
    }
}
