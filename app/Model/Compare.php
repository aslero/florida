<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Compare extends Model
{
    protected $table = 'compares';

    protected $fillable = [
        'title', 'description'
    ];
}
