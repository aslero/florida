<?php

namespace App\Providers;

use App\Model\Contact;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ContactServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(['partials.footer','pages.contacts','pages.about'], function ($view){
            $view->with([
                'contacts' =>  Contact::first(),
            ] );
        });
    }
}
