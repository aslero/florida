<?php

namespace App\Providers;

use App\Model\Domain;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class DomainServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->getDomains();
    }

    public function getDomains(){
        View::composer('layouts.admin', function ($view){
            $view->with([
                'domains' => Domain::orderBy('id', 'asc')->get(),
            ] );
        });
    }
}
