<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePriceComparesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('price_compares', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('price_id');
            $table->foreign('price_id')
                ->references('id')->on('prices')
                ->onDelete('cascade');
            $table->bigInteger('compare_id')->default(0);
            $table->boolean('availability')->default(false);
            $table->string('comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('price_compares');
    }
}
