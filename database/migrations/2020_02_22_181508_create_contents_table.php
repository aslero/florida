<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('pagetitle')->nullable();
            $table->string('description')->nullable();
            $table->string('meta_pagetitle')->nullable();
            $table->string('meta_keywords')->nullable();
            $table->string('meta_description')->nullable();
            $table->mediumText('content')->nullable();
            $table->string('slug')->nullable()->unique();
            $table->string('img')->nullable();
            $table->boolean('published')->default(true);
            $table->boolean('hidemenu')->default(false);
            $table->boolean('bottom')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contents');
    }
}
