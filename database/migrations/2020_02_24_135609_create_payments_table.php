<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->double('cost')->default(0);
            $table->tinyInteger('tariff')->default(0);
            $table->tinyInteger('count_company')->default(0);
            $table->boolean('status')->default(0);
            $table->boolean('arhive')->default(0);
            $table->string('chargeid')->nullable();
            $table->date('date_to')->nullable();
            $table->bigInteger('domain_id')->default(0);
            $table->bigInteger('promocode_id')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
