<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('fullname')->nullable();
            $table->string('pass')->nullable();
            $table->string('phone')->nullable()->unique();
            $table->string('ip')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->date('date_activity')->nullable();
            $table->date('last_online')->nullable();
            $table->boolean('is_confirm')->default(false);
            $table->bigInteger('domain_id')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
