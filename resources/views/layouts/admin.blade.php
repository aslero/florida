<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('/plugins/fontawesome-free/css/all.min.css') }}">
    <link href="{{ asset('/css/admin.css') }}" rel="stylesheet">
    @include('ckfinder::setup')
</head>
<body>
    <div id="app">
        <header class="main-header">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <button type="button" class="nav-link btn-toggle"><i class="fas fa-bars"></i></button>
                </li>
                <li class="nav-item">
                    <button type="button"  data-toggle="modal" data-target="#showDomains" class="btn btn-success">{{\Illuminate\Support\Facades\Cache::get('domain_title')}}</button>
                </li>
            </ul>
        </header>
        <aside class="main-sidebar">
            <a href="" class="brand-logo">
                <img src="{{ url('/assets/img/logo.png') }}" alt="">
            </a>
            <div class="navigation">
                <ul class="nav">
                    <li class="nav-item">
                        <a href="{{route('admin.index')}}" class="nav-link">
                            <i class="nav-icon fas fa-tachometer-alt"></i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('pages.index')}}" class="nav-link">
                            <i class="nav-icon fas fa-book"></i>
                            <span>Pages</span>
                        </a>
                    </li>
                    <li class="nav-header">Modules</li>
                    <li class="nav-item">
                        <a href="{{route('admin.services.index')}}" class="nav-link">
                            <i class="nav-icon fas fa-chart-area"></i>
                            <span>Services</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('admin.reports.index')}}" class="nav-link">
                            <i class="nav-icon fas fa-chalkboard-teacher"></i>
                            <span>Reports</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('reviews.index')}}" class="nav-link">
                            <i class="nav-icon far fa-star"></i>
                            <span>Reviews</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('slider.index')}}" class="nav-link">
                            <i class="nav-icon fab fa-slideshare"></i>
                            <span>Slider</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('admin.faqs.index')}}" class="nav-link">
                            <i class="nav-icon fas fa-question-circle"></i>
                            <span>Faq</span>
                        </a>
                    </li>
                    <li class="nav-header">Payments</li>
                    <li class="nav-item">
                        <a href="{{route('admin.payments.index')}}" class="nav-link">
                            <i class="nav-icon far fa-building"></i>
                            <span>Payments</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('admin.promocodes.index')}}" class="nav-link">
                            <i class="nav-icon fas fa-percent"></i>
                            <span>Promocodes</span>
                        </a>
                    </li>
                    <li class="nav-header">Companies</li>
                    <li class="nav-item">
                        <a href="{{route('admin.companies.index')}}" class="nav-link">
                            <i class="nav-icon far fa-building"></i>
                            <span>Companies</span>
                        </a>
                    </li>
                    <li class="nav-header">Users</li>
                    <li class="nav-item">
                        <a href="{{route('admin.users.index')}}" class="nav-link">
                            <i class="nav-icon fas fa-users"></i>
                            <span>Users</span>
                        </a>
                    </li>
                    <li class="nav-header">Settings</li>
                    <li class="nav-item">
                        <a href="{{route('admin.domains.index')}}" class="nav-link">
                            <i class="nav-icon fas fa-link"></i>
                            <span>Domains</span>
                        </a>
                        <a href="{{route('contacts.index')}}" class="nav-link">
                            <i class="nav-icon far fa-address-card"></i>
                            <span>Contacts</span>
                        </a>
                        <a href="{{route('admin.categories.index')}}" class="nav-link">
                            <i class="nav-icon fas fa-list-ul"></i>
                            <span>Categories</span>
                        </a>
                        <a href="{{route('admin.countries.index')}}" class="nav-link">
                            <i class="nav-icon fas fa-globe-americas"></i>
                            <span>Countries</span>
                        </a>
                        <a href="{{route('admin.cities.index')}}" class="nav-link">
                            <i class="nav-icon fas fa-city"></i>
                            <span>Cities</span>
                        </a>
                    </li>
                    <li class="nav-header">Tariffs</li>
                    <li class="nav-item">
                        <a href="{{route('admin.tariffs.index')}}" class="nav-link">
                            <i class="nav-icon far fa-address-card"></i>
                            <span>Tariffs</span>
                        </a>
                        <a href="{{route('admin.compares.index')}}" class="nav-link">
                            <i class="nav-icon fas fa-globe-americas"></i>
                            <span>Compares</span>
                        </a>
                    </li>
                    <!--<li class="nav-item has-treeview menu-open">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-book"></i>
                            <span>
                                Страницы
                                <i class="right fas fa-angle-left"></i>
                            </span>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <span>Invoice</span>
                                </a>
                            </li>
                        </ul>
                    </li>-->
                </ul>
            </div>
        </aside>
        <div class="wrapper">
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-6">
                            <h1>@yield('h1')</h1>
                        </div>
                    </div>
                </div>
            </div>
            @yield('content')
        </div>
    </div>
    <div class="modal fade" id="showDomains" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="custom__modal--header">
                    <p class="title">List domains</p>
                </div>
                <div class="modal-body">
                    <ul class="list-domains">
                        @forelse($domains as $domain)
                                <li @if($domain->id == \Illuminate\Support\Facades\Cache::get('domain')) active @endif>
                                    <a href="{{route('admin.domain.change',$domain->id)}}">{{ $domain->title }} ({{$domain->domain}})</a></li>
                            @empty
                        @endforelse
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('/js/admin.js') }}"></script>
</body>
</html>
