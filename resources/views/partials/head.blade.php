<!DOCTYPE html>
<html lang="en">
<head>
	<title>Adsense</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="description" content="description">
	<meta name="keywords" content="keywords">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="icon" href="{{ asset('/assets/favicon.ico') }}" type="image/x-icon">
	<link href="{{asset('/assets/style.css')}}" type="text/css" rel="stylesheet">
</head>