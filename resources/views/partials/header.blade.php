<body>
<div id="app">
	<header class="header header_bg">
		<div class="container">
			<div class="header_col">
				<div class="logo"><a href="{{ route('home') }}"><img src="{{ url('/assets/img/logo.png') }}" alt=""></a></div>
				@if(Auth::check())
					<a class="btn_add ntn_s nmt" href="{{route('logout')}}">Log out <i class="fas fa-angle-right"></i></a>
					<a class="btn_add" href="{{ route('dashboard') }}">Dashboard <i class="fas fa-angle-right"></i></a>
					@else
					<a class="btn_add" href="{{route('login')}}">Sign In <i class="fas fa-angle-right"></i></a>
				@endif

				<div class="btn_menu open_menu">
					<div class="menu-icon">
						<span class="menu-icon__line menu-icon__line-left"></span>
						<span class="menu-icon__line"></span>
						<span class="menu-icon__line menu-icon__line-right"></span>
					</div>
				</div>
				<nav class="nav_col">
					<ul>
						<li><a href="{{route('home')}}">Home</a></li>
						<li><a href="{{route('service')}}">Our Service</a></li>
						<li><a href="{{route('reviews')}}">Reviews</a></li>
						<li><a href="{{route('price')}}">Price</a></li>
						<li><a href="{{route('contacts')}}">Contact Us</a></li>
					</ul>
				</nav>
			</div>
		</div>
	</header>