<footer class="footer">
	<div class="container">
		<div class="footer_col">
			<div class="row">
				<div class="footer_box_copyright">
					<img src="{{ url('/assets/img/logo.png') }}" alt="" title="">
					<div class="footer_copyright">
						© 2020 All Rights Reserved. <br>
						<a href="/privacy-policy">Privacy policy</a>
					</div>
					<a href="{{ route('contacts') }}" class="btn">Contact Support <i class="far fa-comment-edit"></i></a>
				</div>
				<div class="footer_box">
					<div class="footer_box_title">Home</div>
					<ul>
						<li><a href="{{ route('home') }}">Home</a></li>
						<li><a href="{{ route('service') }}">Our Service</a></li>
						<li><a href="{{ route('reviews') }}">Reviews</a></li>
						<li><a href="{{ route('price') }}">Price</a></li>
					</ul>
				</div>
				<div class="footer_box">
					<div class="footer_box_title">Company</div>
					<ul>
						<li><a href="{{ route('about') }}">About</a></li>
						<li><a href="{{ route('login') }}">Sign in</a></li>
						<li><a href="{{ route('register') }}">Sign up</a></li>
						<li><a href="{{ route('contacts') }}">Contacts</a></li>
					</ul>
				</div>
				<div class="footer_box">
					<div class="footer_box_title">Contacts</div>
					<ul>
						<li>E-mail: <a href="mailto:{{$contacts->email}}">{{$contacts->email}}</a></li>
						<li>Skype: <a href="skype:{{$contacts->skype}}?chat">{{$contacts->skype}}</a></li>
					</ul>
					<div class="sc_xx_cl">
						<a class="circle_soc_a vk" target="_blank" href="/"><i class="fab fa-facebook-f"></i></a>
						<a class="circle_soc_a face" target="_blank" href="/"><i class="fab fa-twitter"></i></a>
						<a class="circle_soc_a inst" target="_blank" href="/"><i class="fab fa-instagram"></i></a>
						<a class="circle_soc_a you" target="_blank" href="/"><i class="fab fa-youtube"></i></a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="particles-js_a">
		<canvas class="particles-js-canvas-el" width="1349" height="500" style="width: 100%; height: 100%;"></canvas>
	</div>
</footer>
<div class="line_z"></div>
</div>
<script src="{{ asset('/assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('/assets/js/particles.js') }}"></script>
<link href="{{ asset('/assets/css/animate.css') }}" rel="stylesheet">
<link href="{{ asset('/assets/css/all.min.css') }}" rel="stylesheet">
<script src="{{ asset('/assets/js/slick.min.js') }}"></script>
<script src="{{ asset('/assets/js/main.js') }}"></script>
<script src="{{ asset('/assets/js/jquery.fancybox.min.js') }}"></script>
<link href="{{ asset('/assets/css/jquery.fancybox.min.css') }}" rel="stylesheet">
<script src="{{ asset('/assets/js/wow.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.jquery.min.js"></script>
<script type="text/javascript">
    $(".livesearch").chosen();
</script>


<script src="{{ asset('/js/app.js') }}"></script>
<!-- Start of adsinflorida Zendesk Widget script -->
<script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=93f485d9-8bfd-44dd-8eef-fb1f5dec5752"> </script>
<!-- End of adsinflorida Zendesk Widget script -->
<script>
	new WOW().init();
</script>
</body>
</html>