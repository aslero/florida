<div class="sidebar_user">
	<div class="profile_block">
		<div class="content">
			<div class="txt">Welcome</div>
			<div class="title_u"><a href="/profile">{{Auth::user()->fullname}}</a></div>
			<div class="icons">
				<a href="/profile/logout"><i class="fas fa-sign-out"></i> Log out</a>
			</div>
		</div>
	</div>
	<nav class="menu">
		<ul>
			<li {{ Request::is('profile/dashboard') ? 'class=active' : '' }}><a href="{{ route('dashboard') }}"><i class="far fa-desktop-alt"></i> Dashboard</a></li>
			<li {{ Request::is('profile/business') ? 'class=active' : '' }}><a href="{{ route('business') }}"><i class="far fa-business-time"></i> Businesses</a></li>
			<li {{ Request::is('profile') ? 'class=active' : '' }}><a href="{{ route('profile') }}"><i class="fas fa-user"></i> My Account</a></li>
			<li><a href="/profile/logout"><i class="fas fa-sign-out"></i> Log out</a></li>
		</ul>
	</nav>
	<div id="particles-js_b">
		<canvas class="particles-js-canvas-el" width="1349" height="500" style="width: 100%; height: 100%;"></canvas>
	</div>
</div>