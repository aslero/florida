<div class="sidebar-overlay"></div>
<div class="sidebar">
	<div class="close"><img src="{{ url('/assets/img/close.png') }}" alt="Close"></div>
	<div class="container">
		<div class="sidebar-content">
			<ul>
				<li><a href="{{ route('home') }}">Home</a></li>
				<li><a href="{{ route('service') }}">Our Service</a></li>
				<li><a href="{{ route('reviews') }}">Reviews</a></li>
				<li><a href="{{ route('price') }}">Price</a></li>
				<li><a href="{{ route('contacts') }}">Contact Us</a></li>
			</ul>
		</div>
	</div>
</div>