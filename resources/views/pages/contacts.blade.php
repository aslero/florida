@include('partials.head')
@include('partials.header')
@include('partials.menu')

<div class="margin_col mrt95">
	<div class="speedbar">
		<div class="container relative">
			<div class="speedbar_inner">
				<a href="{{ route('home') }}">Home</a> <i class="fas fa-angle-right"></i> Contact us
			</div>
		</div>
	</div>
	<div class="white_col pdt100">
		<div class="container">
			<div class="txt_contacts wow fadeIn" data-wow-duration="0.3s" data-wow-delay="0.1s">
				<div class="title">{{$content->pagetitle}}</div>
				<div class="descr">
					{!! $content->content !!}

				</div>
				<div class="phone_head no_mob_contact">
					<div class="circles_cols phs"> <i style="color: #33f1ac;" class="far fa-at"></i> </div>
					{{$contacts->email}}
					<div class="schedule">
						<div>{{$contacts->email_work}}</div>
					</div>
				</div>
				<div class="phone_head no_mob_contact">
					<div class="circles_cols"><i style="color: #0e8fef;"  class="fab fa-skype"></i></div>
					{{$contacts->skype}}
					<div class="schedule">
						<div>{{$contacts->skype_work}}</div>
					</div>
				</div>
			</div>
			<form action="{{ route('contacts.send') }}" method="POST" class="contacts_post wow fadeIn" id="contacts" data-wow-duration="0.3s" data-wow-delay="0.2s">
				@csrf
				<div class="input_group">
					<div class="icon"><i class="fal fa-user"></i></div>
					<input class="input" type="text" name="name" required placeholder="Your name">
				</div>
				<div class="input_group">
					<div class="icon"><i class="fal fa-envelope"></i></div>
					<input class="input" type="email" name="email" required placeholder="Work email">
				</div>
				<div class="input_group">
					<div class="icon"><i class="fal fa-comment-lines"></i></div>
					<textarea class="input" name="message" required placeholder="Message"></textarea>
				</div>
				<div class="cheker">
					<label>
					<input type="checkbox" required name="police" checked="">
					<span></span>
					<small class="rmb">I agree to the <a href="#">Terms of Service & Privacy Policy</a></small>
					</label>
					<br><br>
					<div class="txt">Too long? We don't share your data and we won't send you marketing emails. We will however send you product messages to help you get the most from Adsinflorida.com</div>
				</div>
				<input type="submit" name="submit" class="btn_submit" value="Send Message">
			</form>
		</div>
	</div>
</div>

@include('partials.footer')