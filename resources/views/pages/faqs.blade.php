@include('partials.head')
@include('partials.header')
@include('partials.menu')

<div class="margin_col mrt95">
  <div class="speedbar">
	<div class="container relative">
	  <div class="speedbar_inner">
		<a href="{{ route('home') }}">Home</a>
		<i class="fas fa-angle-right"></i> Reviews
	  </div>
	</div>
  </div>
  <div class="white_col pdt100">
	<div class="container">
	  <div class="title_col_a">
		<h1 class="title">Reviews</h1>
	  </div>
		<div class="slider slick_rew mrtm40">
			@forelse($reviews as $review)
				<div>
					<div class="item_reviews">
						<div class="rating">
							@for($i=1;$i<6;$i++)
								<i class="fas fa-star @if($i <= $review->raiting) good @endif" @if($i == $review->raiting) last="final" @endif></i>
							@endfor
						</div>
						<div class="descr">
							{{$review->text}}
						</div>
						<div class="name">{{$review->name}}, {{\Illuminate\Support\Carbon::parse($review->created_at)->format('d.m.Y')}}</div>
					</div>
				</div>
				@empty
			@endforelse
		</div>

		<div class="title_col_a mty100 mrt100">
		<div class="title">Add reviews</div>
	  </div>
	  <div class="center_block">
	  	<form action="{{ route('reviews.store') }}" method="post" class="ajaxForm" id="reviews">
			<div class="container_add_reviews">
			  <div class="company_box_lk wd100">
				<div class="input_group mrt30">
					<div class="icon"><i class="fal fa-user"></i></div>
					<input class="input" type="text" name="name" placeholder="Your name">
					<label class="info">Your Name</label>
				</div>
				<div class="input_group mrt30">
					@csrf
					<div class="icon"><i class="fal fa-at"></i></div>
					<input class="input" type="email" name="email" placeholder="Email">
					<label class="info">Email</label>
				</div>
				<div class="input_group mrt30">
					<div class="icon"><i class="fal fa-comments"></i></div>
					<textarea class="input" placeholder="Comments" name="comment"></textarea>
					<label class="info">Comments</label>
				</div>
				<div class="rating ius">
					<div class="rt_t">Rating service</div>
					<i class="fas fa-star"></i>
					<i class="fas fa-star"></i>
					<i class="fas fa-star"></i>
					<i class="fas fa-star"></i>
					<i class="fas fa-star"></i>
				</div>
				<input type="hidden" name="raiting">
				<div class="center_btn mt20">
					<input type="submit" name="submit" class="btn_submit bg_cs" value="Add Review">
				</div>
			  </div>
			</div>
		</form>
	  </div>
	</div>
  </div>
</div>



@include('partials.footer')

<style type="text/css">
	.rating i:hover {
    	color: #ff194e;
	}
	.rating {
		cursor: default;
	}
	.fa-star {
		cursor: pointer;
	}
</style>

<script type="text/javascript">
	
	$( document ).ready(function() {


		$('.fa-star').on('mouseover mouseout click', function(event) {

			var $e = $(this);

			if ($e.attr('last') == 'final') {
				return;
			}

			$('.fa-star[last="final"]').removeAttr('last');

			if(event.type == 'click') {
				$('.fa-star').removeAttr('last');
				$e.attr('last', 'selected');
				console.log($('input[name="raiting"]').val($('.fa-star.good').length));
			}
			if(event.type == 'mouseout') {
				if ($e.attr('last') == 'selected') {
					$('.fa-star').removeAttr('last');
					$e.attr('last', 'final');
					return;
				}
				$('.fa-star').removeClass('good');
				return;
			}

			$('.fa-star').removeClass('good');
			//$('.fa-star').removeAttr('last');

			$e.addClass('good');
			$prev = $e.prev();
			$next = $e.next();

			for (i = 0; i <= 4; i++) {
				if ($prev.length != 0 && $prev.hasClass('fa-star')) {
					$prev.addClass('good');
					$prev = $prev.prev();
				}
				if ($next.length != 0 && $next.hasClass('fa-star')) {
					$prev.removeClass('good');
					$next = $next.next();
				}
			}
		});








/*		$('input, textarea').on('focus focusout', function(event) {

			$e = $(this);

			if (event.type == 'focus') {
				$placeholder = $e.attr("placeholder");
				$e.attr("ph-input", $placeholder);
				$placeholder = '';
			}
			if (event.type == 'focusout') {
				$placeholder = $e.attr("ph-input");
			}
			$e.attr("placeholder", $placeholder);
		});*/
	});

</script>