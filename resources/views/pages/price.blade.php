@include('partials.head')
@include('partials.header')
@include('partials.menu')

<div class="margin_col mrt95">
  <div class="speedbar">
    <div class="container relative">
      <div class="speedbar_inner">
        <a href="{{ route('home') }}">Home</a> <i class="fas fa-angle-right"></i> Price
      </div>
    </div>
  </div>
  <div class="white_col pdt100">
    <div class="container">
      <div class="title_col_a">
        <h1 class="title">Price</h1>
      </div>
      <div class="table_col_p">
        <ul>
          @forelse($tariffs as $row=>$tariff)
            <li class="@if($row == 0) bg-purple active_a @else @if($tariff->special == 1) bg-blue @endif @endif">
              <button>{{$tariff->title}}</button>
            </li>
          @empty
          @endforelse
        </ul>
        <table>
          <thead>
          <tr>
            <th class="hide"></th>
            @forelse($tariffs as $row=>$tariff)
              @if($row == 0)
                <th class="bg_table">
                  <div class="bg-free default">{{$tariff->title}}</div>
                </th>
              @else
                @if($tariff->special == 1)
                  <th class="bg_sp brt4 bg_table">
                    <div class="bg-blue">{{$tariff->title}}</div>
                  </th>
                @else
                  <th class="bg_hide brdr_n bg_table">
                    <div class="bg-promo">{{$tariff->title}}</div>
                  </th>
                @endif
              @endif
            @empty
            @endforelse
          </tr>
          </thead>
          <tbody>
          <tr>
            <td>Price</td>
            @forelse($tariffs as $row=>$tariff)
              @if($row == 0)
                <td class="default">
                  <span class="txt-top" style="color: #bdd5e8;">$</span>
                  <span style="color: #bdd5e8;" class="txt-l">{{$tariff->price}}</span>
                </td>
              @else
                @if($tariff->special == 1)
                  <td class="bg_sp">
                    <span class="txt-top txt_lt">$</span><span class="txt-l txt_lt">{{$tariff->price}}/year</span>
                  </td>
                @else
                  <td class="bg_hide">
                    <span class="txt-top">$</span><span class="txt-l">{{$tariff->price}}/year</span>
                  </td>
                @endif
              @endif
            @empty
            @endforelse
          </tr>
          @forelse($compares as $compare)
            <tr>
              <td>{{$compare->title}}</td>
              @forelse($compare->tariff as $row=>$tcompare)
                <td class="@if ($row == 0) default @else @if($tcompare->special == 1) bg_sp @else bg_hide @endif @endif ">
                  @if($tcompare->availability == 0)
                    @if($tcompare->comment)
                      <div class="intro">{{$tcompare->comment}}</div>
                    @else
                      —
                    @endif
                  @else
                    @if($tcompare->comment)
                      <div class="intro">{{$tcompare->comment}}</div>
                    @else
                      <div class="img_tick"><i class="fas fa-check-circle"></i></div>
                    @endif
                  @endif
                </td>
              @empty
              @endforelse
            </tr>
          @empty
          @endforelse
          <tr>
            <td></td>
            @forelse($tariffs as $row=>$tariff)
              @if ($row == 0)
                <td class="default">
                  @if(Auth::check())
                   <div class="btn_s modal_info"  data-price="{{$tariff->price}}" data-id="{{$tariff->id}}" data-box="gift">Select</div>
                    @else
                    <a href="/register?tariff={{$tariff->id}}" class="btn_s">Select</a>
                  @endif
                </td>
              @else
                @if($tariff->special == 1)
                  <td class="bg_sp brb4 bg_sp_nobr">
                    @if(Auth::check())
                    <div class="btn_a modal_info" data-price="{{$tariff->price}}" data-id="{{$tariff->id}}" data-box="gift">Select</div>
                    @else
                      <a href="/register?tariff={{$tariff->id}}" class="btn_s">Select</a>
                    @endif
                  </td>
                @else
                  <td class="bg_hide  brb4 bg_sp_nobr">
                    @if(Auth::check())
                    <div class="btn modal_info" data-price="{{$tariff->price}}" data-id="{{$tariff->id}}" data-box="gift">Select</div>
                    @else
                      <a href="/register?tariff={{$tariff->id}}" class="btn_s">Select</a>
                    @endif
                  </td>
                @endif
              @endif
            @empty
            @endforelse

          </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="drop_info_up" id="gift">
  <div class="drop_info_close"></div>
  <div class="data_drop_col">
    <div class="title">Buy on Tarif Plan</div>
    <div class="content_user">
      <div class="company_box_lk wd100">
        <div class="tarification_checkout">
          <div class="info">
            <span>Client:</span>
            <div class="summ">DemiGroup</div>
          </div>
          <div class="info hidden" data-id="2">
            <span>Tarification:</span>
            <div class="btn_p">Premium</div>
          </div>
          <!--<div class="info">
            <span>Tarification:</span>
            <div class="btn_f">Free</div>
          </div>-->
          <div class="info hidden" data-id="1">
            <span>Tarification:</span>
            <div class="btn_ps">Special</div>
          </div>
          <div class="info">
            <span>Payment:</span>
            <div class="summ">$<span class="check--sum">225</span>/year</div>
          </div>
        </div>
        <div class="row">
          <div class="clr"></div>
          <div class="wd50_inp">
            <div class="input_group">
              <div class="icon"><i class="fal fa-tags"></i></div>
              <input class="input" type="text" id="promocode" name="text" placeholder="Promo code...">
              <label class="info">promo code</label>
            </div>
          </div>
          <div class="wd50_inp">
            <input type="button" name="submit" id="btn_promo_aply" class="btn_submit bg_cs_apply" value="Apply Code">
          </div>
        </div>
        <div class="total_pay">Total Pay: $<span class="check--sum">225</span></div>
        <div class="center_btn mt20">
          <form action="{{route('payments')}}" method="GET" id="check--from">
            <input type="hidden" name="promocode" value="">
            <input type="hidden" name="price" value="">
            <input type="hidden" name="id" value="">
            <input type="submit" name="submit" class="btn_submit bg_cs" value="Checkout">
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal_overlay"></div>

@include('partials.footer')