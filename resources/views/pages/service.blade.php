@include('partials.head')
@include('partials.header')
@include('partials.menu')

<div class="margin_col mrt95">
	<div class="speedbar">
		<div class="container relative">
			<div class="speedbar_inner">
				<a href="{{ route('home') }}">Home</a> <i class="fas fa-angle-right"></i> Sites
			</div>
		</div>
	</div>
	<div class="white_col pdt100">
		<div class="container">
			<div class="title_col_a mrb60">
				<h1 class="title">Sites Company</h1>
			</div>
			<div class="tab">
				<div class="tab_over_s">
					<ul class="tabs">
						@forelse($services as $row=>$service)
							<li class=" @if($row == 0) current @endif  no_disp_offer"><a href="#tab{{$row}}">{{$service->label}}  <i class="fas fa-long-arrow-down"></i></a></li>
						@empty
						@endforelse
						<div class="center">
							<div class="btn_b btn_more">Show more</div>
						</div>
					</ul>
				</div>
				<div class="tab_content wow fadeIn" data-wow-duration="0.4s" data-wow-delay="0.1s">
					@forelse($services as $row=>$service)
						<div id="tab{{$row}}" class="tabs_item" @if($row == 0)  style="display: block;" @endif>
							<div class="site_col">
								<div class="img"><img src="{{$service->img}}" alt=""></div>
								<div class="content">
									<a href="{{$service->link}}" target="_blank" class="title">{{$service->label}} <i class="far fa-external-link"></i></a>
									<div class="descr">{{$service->description}}</div>
									<div class="title">Stats</div>
									<div class="counts_xx_block">
										<div class="row">
											@forelse($service->stats as $stat)
												<div class="couts_xx_col wow fadeIn" data-wow-duration="0.4s" data-wow-delay="0.1s">
													<div class="couts_go_box">
														<div class="content_count">
															<div class="count no_before">{{$stat->value}}</div>
															<span>{{$stat->title}}</span>
														</div>
													</div>
												</div>
											@empty
											@endforelse
										</div>
									</div>
								</div>
							</div>
						</div>
					@empty
					@endforelse
				</div>
			</div>
		</div>
	</div>
</div>

@include('partials.footer')