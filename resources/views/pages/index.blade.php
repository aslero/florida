@include('partials.head')
@include('partials.header')
@include('partials.menu')

<div class="margin_col">
	<div class="home_col">
		<div class="container relative">
			<div class="home_box">
				<h1 class="title wow fadeIn" data-wow-duration="0.3s" data-wow-delay="0.1s">{{$slider->pagetitle}}</h1>
				<p class="descr wow fadeIn" data-wow-duration="0.3s" data-wow-delay="0.2s">
					{{$slider->description}}
				</p>
				<!-- <div class="btn wow fadeIn" data-wow-duration="0.3s" data-wow-delay="0.3s">BUILD MY AD CAMPAIGN <i class="fas fa-angle-right"></i></div> -->
				<a href="{{ route('add-company') }}" class="btn wow fadeIn" data-wow-duration="0.3s" data-wow-delay="0.3s">BUILD MY AD CAMPAIGN <i class="fas fa-angle-right"></i></a>
			</div>
		</div>
		<div id="particles-js">
			<canvas class="particles-js-canvas-el" width="1349" height="500" style="width: 100%; height: 100%;"></canvas>
		</div>
	</div>
	<div class="white_col">
		<div class="container">
			<div class="title_col wow fadeIn" data-wow-duration="0.3s" data-wow-delay="0s">
				<div class="numb">01</div>
				<div class="title">How AdsinFL Work</div>
			</div>
			<div class="adsfl_work_col wow fadeIn" data-wow-duration="0.3s" data-wow-delay="0.1s">
				<div class="adsfl_work_box">
					<div class="img"><i style="color: #33f1ac;" class="far fa-check-double"></i></div>
					<div class="title">Green</div>
					<div class="descr">Everything is On Track and no action is needed at this point in time. You are doing a good job!</div>
				</div>
			</div>
			<div class="adsfl_work_col wow fadeIn" data-wow-duration="0.3s" data-wow-delay="0.2s">
				<div class="adsfl_work_box">
					<div class="img"><i style="color: #886af9;" class="far fa-exclamation-triangle"></i></div>
					<div class="title">Amber</div>
					<div class="descr">You should watch this item and ensure that it does not turn red. No immediate action needed.</div>
				</div>
			</div>
			<div class="adsfl_work_col wow fadeIn" data-wow-duration="0.3s" data-wow-delay="0.3s">
				<div class="adsfl_work_box">
					<div class="img"><i style="color: #ff4872;" class="far fa-engine-warning"></i></div>
					<div class="title">Red</div>
					<div class="descr">This item requires your attention as action needs to be taken imminently.</div>
				</div>
			</div>
		</div>
	</div>
	<div class="gray_col">
		<div class="container">
			<div class="title_col wow fadeIn">
				<div class="numb">02</div>
				<div class="title">Reporting for the way you work</div>
			</div>
			@forelse($reports as $row=>$report)
				<div class="reporting_col @if(($row % 2) == 0) rep_right @endif">
					<div class="content_a wow fadeInLeft" data-wow-duration="0.3s" data-wow-delay="0.1s">
						<div class="img"><i class="far fa-sync-alt"></i></div>
						<div class="title">{{$report->label}}</div>
						<div class="descr">{{$report->description}}</div>
						<a href="{{ route('price') }}" class="btn">BUY NOW <i class="fas fa-angle-right"></i></a>
						<!-- <div class="btn">BUY NOW <i class="fas fa-angle-right"></i></div> -->
					</div>
					<div class="content_b wow fadeInRight" data-wow-duration="0.3s" data-wow-delay="0.2s">
						<div class="row">
							@forelse($report->galleries as $stat)
								<div class="img">
									<img src="{{$stat->image}}" alt="{$stat->description}}">
									<div class="title">{{$stat->description}}</div>
								</div>
								@empty
							@endforelse
						</div>
					</div>
				</div>
				@empty
			@endforelse
		</div>
	</div>
	<div class="white_col">
		<div class="container">
			<div class="title_col wow fadeIn" data-wow-duration="0.4s" data-wow-delay="0.1s">
				<div class="numb">03</div>
				<div class="title">We are different</div>
			</div>
			@forelse($faqs as $row=>$faq)
				<div class="spoiler mrt40 wow fadeIn @if($row === 0) is-active @endif" data-wow-duration="0.4s" data-wow-delay="0.1s">
					<div class="spoiler-title @if($row === 0) active @endif">
						<div class="img">{!! $faq->icon !!}</div>
						{{$faq->title}}
					</div>
					<div class="spoiler-content">
						<p>{{$faq->description}}</p>
					</div>
				</div>
				@empty
			@endforelse
		</div>
	</div>
	<div class="gray_col">
		<div class="container">
			<div class="title_col wow fadeIn">
				<div class="numb">04</div>
				<div class="title">Our Service</div>
			</div>
			<div class="tab">
				<div class="tab_over_s">
					<ul class="tabs wow fadeIn" data-wow-duration="0.4s" data-wow-delay="0.1s">
						@forelse($services as $row=>$service)
							<li class=" @if($row == 0) current @endif  no_disp_offer"><a href="#tab{{$row}}">{{$service->label}}  <i class="fas fa-long-arrow-down"></i></a></li>
							@empty
						@endforelse
						<div class="center">
							<div class="btn_b btn_more">Show more</div>
						</div>
					</ul>
				</div>
				<div class="tab_content wow fadeIn" data-wow-duration="0.4s" data-wow-delay="0.1s">
					@forelse($services as $row=>$service)
						<div id="tab{{$row}}" class="tabs_item" @if($row == 0)  style="display: block;" @endif>
							<div class="site_col">
								<div class="img"><img src="{{$service->img}}" alt=""></div>
								<div class="content">
									<a href="{{$service->link}}" target="_blank" class="title">{{$service->label}} <i class="far fa-external-link"></i></a>
									<div class="descr">{{$service->description}}</div>
									<div class="title">Stats</div>
									<div class="counts_xx_block">
										<div class="row">
											@forelse($service->stats as $stat)
												<div class="couts_xx_col wow fadeIn" data-wow-duration="0.4s" data-wow-delay="0.1s">
													<div class="couts_go_box">
														<div class="content_count">
															<div class="count no_before">{{$stat->value}}</div>
															<span>{{$stat->title}}</span>
														</div>
													</div>
												</div>
												@empty
											@endforelse
										</div>
									</div>
								</div>
							</div>
						</div>
						@empty
					@endforelse
				</div>
			</div>
		</div>
	</div>
	<div class="white_col">
		<div class="container">
			<div class="title_col wow fadeIn" data-wow-duration="0.3s" data-wow-delay="0.1s">
				<div class="numb">05</div>
				<div class="title">Real businesses. Real software. Real reviews.</div>
			</div>
			<div class="slider slick_rew">
				@forelse($reviews as $review)
					<div>
						<div class="item_reviews">
							<div class="rating">
								@for($i=1;$i<6;$i++)
									<i class="fas fa-star @if($i <= $review->raiting) good @endif" @if($i == $review->raiting) last="final" @endif></i>
								@endfor
							</div>
							<div class="descr">
								{{$review->text}}
							</div>
							<div class="name">{{$review->name}}, {{\Illuminate\Support\Carbon::parse($review->created_at)->format('d.m.Y')}}</div>
						</div>
					</div>
					@empty
				@endforelse
			</div>
		</div>
	</div>
</div>

@include('partials.footer')