@include('partials.head')
@include('partials.header')
@include('partials.menu')
<div class="margin_col mrt95">
    <div class="speedbar">
        <div class="container relative">
            <div class="speedbar_inner">
                <a href="{{ route('home') }}">Home</a>
                <i class="fas fa-angle-right"></i> Success
            </div>
        </div>
    </div>
    <div class="white_col pdt100">
        <div class="container">
            <div class="title_col_a">
                <h1 class="title">Order tariff</h1>
            </div>
            <div class="content_user">
                @if(Session::has('error'))
                    {{ Session::get('error') }}
                @endif
                <form class="form-horizontal" method="POST" id="payment-form" role="form" action="{!!route('payments.pay')!!}" >
                    {{ csrf_field() }}
                    <div class="total_pay">Total Pay: ${{$price}}</div>
                    <input type="hidden" name="id" value="{{$id}}">
                    <input type="hidden" name="promocode" value="{{$promocode}}">
                    <input class='form-control card-expiry-year' placeholder='YYYY' size='2' type='hidden' name="amount" value="{{$price}}">
                    <div class="input_group mrt30">
                        <div class="icon"><i class="fal fa-users"></i></div>
                        <input id="name" type="text" class="input " name="card_no" size='20' required="" autocomplete="off" value="">
                        <label class="info">Cart number</label>
                    </div>
                    <div class="input_group mrt30">
                        <div class="icon"><i class="fal fa-users"></i></div>
                        <input id="name" type="text" class="input " name="cvvNumber" required="" placeholder='123' size='4' autocomplete="off" value="">
                        <label class="info">CVV</label>
                    </div>
                    <div class="input_group mrt30">
                        <div class="icon"><i class="fal fa-users"></i></div>
                        <input id="name" type="text" class="input " name="ccExpiryMonth" required="" size='4' placeholder='MM' autocomplete="off" value="">
                        <label class="info">MM</label>
                    </div>
                    <div class="input_group mrt30">
                        <div class="icon"><i class="fal fa-users"></i></div>
                        <input id="name" type="text" class="input " name="ccExpiryYear" required="" size='2' placeholder='YY' autocomplete="off" value="">
                        <label class="info">YY</label>
                    </div>
                    <button class='btn_submit bg_cs' type='submit'>Pay</button>
                </form>
            </div>
        </div>
    </div>
</div>
@include('partials.footer')
