@include('partials.head')
@include('partials.header')
@include('partials.menu')
<div class="margin_col mrt95">
    <div class="speedbar">
        <div class="container relative">
            <div class="speedbar_inner">
                <a href="{{ route('home') }}">Home</a>
                <i class="fas fa-angle-right"></i> Success
            </div>
        </div>
    </div>
    <div class="white_col pdt100">
        <div class="container">
            <div class="title_col_a">
                <h1 class="title">Payment success</h1>
            </div>
            <div class="information">
                <p class="center">You have successfully purchased a tariff</p>
            </div>
        </div>
    </div>
</div>
@include('partials.footer')