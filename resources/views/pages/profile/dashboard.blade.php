@include('partials.head')
@include('partials.header')
@include('partials.menu')

<div class="margin_col mrt95">
	<div class="speedbar">
		<div class="container relative">
			<div class="speedbar_inner">
				<a href="{{route('home')}}">Home</a> <i class="fas fa-angle-right"></i> Statistics
			</div>
		</div>
	</div>
	<div class="white_col pdt100">
		<div class="container">
			<div class="title_col_a mrb60">
				<h1 class="title">Statistics</h1>
			</div>
			<div class="user_page_col">
				
				@include('partials.profile.sidebar')

				<div class="content_user">
					<div class="company_box_lk wd100">
						@forelse($companies as $row=>$company)
							<div class="spoiler mrt40 wow fadeIn @if($row == 0) is-active @endif" data-wow-duration="0.4s" data-wow-delay="0.1s">
								<div class="spoiler-title active">
									<div class="img"><i class="far fa-sensor-on"></i></div>
									{{$company->name}}
								</div>
								<div class="spoiler-content no_pad_spoiler">
									<div class="tab tab_spolier_stat">
										<ul class="tabs">
											<div class="row">
												<li class="current stat_tab"><span data-id="{{$company->id}}" data-days="1">Day  <i class="fas fa-long-arrow-down"></i></span></li>
												<li class="stat_tab"><span data-id="{{$company->id}}" data-days="7">Week <i class="fas fa-long-arrow-down"></i></span></li>
												<li class="stat_tab"><span data-id="{{$company->id}}" data-days="30">Month <i class="fas fa-long-arrow-down"></i></span></li>
												<li class="stat_tab"><span data-id="{{$company->id}}" data-days="365">Year<i class="fas fa-long-arrow-down"></i></span></li>
											</div>
										</ul>
										<div class="tab_content no_pad_s">
											<div id="tab1" class="tabs_item" style="display: block;">
												<div class="content_scroll_t">
													<div class="table_div_stat">
														<div class="item_t bg_h brdl">
															Site<br>
															<div class="sts">{{$company->sites}}</div>
														</div>
														<div class="item_xt bg_h">
															Views user <br>
															<div class="sts">{{$company->users}}</div>
														</div>
														<div class="item_xt bg_h">
															Status <br>
															<div class="sts">{{$company->on}} on / {{$company->off}} off</div>
														</div>
														<div class="item_xt bg_h brdr">
															Subscription <br>
															<div class="sts">6 Free / 6 Premium</div>
														</div>
													</div>
													@forelse($company->stats as $stat)
														<div class="table_div_stat">
															<div class="item_t">
																<a href="{{$stat->link}}" target="_blank">{{$stat->name}}</a>
															</div>
															<div class="item_xt">
																<div class="cout">{{$stat->user}}</div>
															</div>
															<div class="item_xt">
																@if($stat->status == 1)
																	<div class="atc">Active</div>
																	@else
																	<div class="atc_o">Off</div>
																@endif
															</div>
															<div class="item_xt">
																@if($stat->status == 0)
																	<div class="subs">Free</div>
																@else
																	<div class="subs_p">Premium</div>
																@endif

															</div>
														</div>
														@empty
													@endforelse
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							@empty
						@endforelse
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@include('partials.footer')