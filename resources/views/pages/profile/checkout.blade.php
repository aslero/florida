@include('partials.head')
@include('partials.header')
@include('partials.menu')

<div class="margin_col mrt95">
	<div class="speedbar">
		<div class="container relative">
			<div class="speedbar_inner">
				<a href="{{ route('home') }}">Home</a> <i class="fas fa-angle-right"></i> User page <i class="fas fa-angle-right"></i> Checkout
			</div>
		</div>
	</div>
	<div class="white_col pdt100">
		<div class="container">
			<div class="title_col_a mrb60">
				<h1 class="title">Checkout</h1>
			</div>
			<div class="user_page_col">
				
				@include('partials.profile.sidebar')

				<div class="content_user">
					<div class="company_box_lk wd100">
						<div class="tarification_checkout">
							<div class="info"><span>Client:</span> <div class="summ">DemiGroup</div></div>
							<div class="info"><span>Tarification:</span> <div class="btn_p">Premium</div></div>
							<div class="info"><span>Tarification:</span> <div class="btn_f">Free</div></div>
							<div class="info"><span>Tarification:</span> <div class="btn_ps">Special</div></div>
							<div class="info"><span>Payment:</span> <div class="summ">$225/year</div></div>
						</div>

					<div class="row">
						<div class="clr"></div>
						<div class="wd50_inp">
						<div class="input_group">
							<div class="icon"><i class="fal fa-tags"></i></div>
							<input class="input" type="text" name="text" placeholder="Promo code...">
							<label class="info">promo code</label>
						</div>
					</div>
					<div class="wd50_inp">
						<input type="submit" name="submit" class="btn_submit bg_cs_apply" value="Apply Code">
					</div>
				</div>
				<div class="total_pay">Total Pay: $225</div>

						<div class="center_btn mt20">
							<input type="submit" name="submit" class="btn_submit bg_cs" value="Checkout">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@include('partials.footer')