@include('partials.head')
@include('partials.header')
@include('partials.menu')

<div class="margin_col mrt95">
	<div class="speedbar">
		<div class="container relative">
			<div class="speedbar_inner">
				<a href="{{ route('home') }}">Home</a> <i class="fas fa-angle-right"></i> User page <i class="fas fa-angle-right"></i> My Company
			</div>
		</div>
	</div>
	<div class="white_col pdt100">
		<div class="container">
			<div class="title_col_a mrb60">
				<h1 class="title">My Company</h1>
			</div>
			<div class="user_page_col">

				@include('partials.profile.sidebar')

				<div class="content_user">
					<div class="company_box_lk wd100">
						<div class="row">
							@forelse($companies as $company)
								<div class="comp_ave_col">
									<div class="comp_ave_box premium_box" data-id="{{$company->id}}">
										<a href="#" class="img" style="background: url(/storage/{{$company->image}});background-size: cover;background-position: center;">
											<button type="button" data-id="{{$company->id}}" class="del delete-company">Delete <i class="fas fa-trash-alt"></i></button>
											@if($company->price_id > 0)
												<div class="premium_ticker">Premium Company</div>
											@endif
										</a>
										<div class="content">
											<a href="#" class="title">{{$company->name}}</a>
											<div class="category"><a href="#">{{$company->category->title}}</a></div>
											<div class="contact">
												<div class="info"><i class="fal fa-address-book"></i>  @if($company->zip) {{$company->zip}}, @endif @if($company->city_id > 0) {{$company->cities->title}}, @endif  @if($company->address) {{$company->address}}@endif</div>
											</div>
											<div class="contact">
												<a class="tel" href="#"><i class="fal fa-phone"></i> {{$company->phone}} </a>
											</div>
											<div class="contact">
												<a class="tel" href="#"><i class="fal fa-clock"></i> {{$company->time_am}} AM - {{$company->time_pm}} PM </a>
											</div>
											<div class="contact mb0">
												<a class="tel" href="{{$company->website}}" target="_blank"><i class="fal fa-globe"></i> {{$company->website}} </a>
											</div>
											<a href="{{ route('dashboard') }}" class="btn">Stats <i class="fas fa-angle-right"></i></a>
											<a href="{{ route('edit-company', $company->id) }}" class="btn_a">Edit <i class="fas fa-edit"></i></a>
											<!-- <div class="btn_a">Edit <i class="fas fa-edit"></i></div> -->
										</div>
									</div>
								</div>
								@empty
							@endforelse
						</div>
					</div>
					<div class="center">
						<a href="{{ route('add-company') }}" class="btn_b ">Add company</a>
						<!-- <div class="btn_b btn_more">Add company</div> -->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@include('partials.footer')