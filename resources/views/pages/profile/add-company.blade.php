@include('partials.head')
@include('partials.header')
@include('partials.menu')

<div class="margin_col mrt95">
	<div class="speedbar">
		<div class="container relative">
			<div class="speedbar_inner">
				<a href="{{ route('home') }}">Home</a> <i class="fas fa-angle-right"></i> User page <i class="fas fa-angle-right"></i> Add Company
			</div>
		</div>
	</div>
	<div class="white_col pdt100">
		<div class="container">
			<div class="title_col_a mrb60">
				<h1 class="title">Add Company</h1>
			</div>
			<div class="user_page_col">
				
				@include('partials.profile.sidebar')

				<div class="content_user">
					<form method="POST" action="{{route('company.store')}}" enctype="multipart/form-data" class="company_box_lk wd100">
						@csrf
						<div class="input_group mrt30">
							<div class="icon"><i class="fal fa-users"></i></div>
							<input id="name" type="text" class="input @error('name') is-invalid @enderror" name="name" required autocomplete="name" placeholder="Company Name..."  value="{{ old('name') }}">
							<label class="info">Company name</label>
							@error('name')
								<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
						</div>
						<div class="input_group">
							<div class="icon"><i class="fal fa-marker"></i></div>
							<select class="input" name="category_id">
								<option value="0" @if (old('category_id') == 0) selected @endif>Select type company</option>
								@forelse($categories as $category)
									<option value="{{$category->id}}" @if (old('category_id') == $category->id) selected @endif>{{$category->title}}</option>
									@empty
								@endforelse
							</select>
							<label class="info">Type Company</label>
							@error('category_id')
							<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
						</div>
						<div class="input_group">
							<div class="icon"><i class="fal fa-map-marker-alt"></i></div>
							<select class="input @error('city_id') is-invalid @enderror livesearch" name="city_id">
								<option value="0" @if (old('city_id') == 0) selected @endif>Select city</option>
								@forelse($cities as $city)
									<option value="{{$city->id}}" @if (old('city_id') == $city->id) selected @endif>{{$city->title}}</option>
								@empty
								@endforelse
							</select>
							@error('city_id')
							<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
						</div>
						<div class="input_group">
							<div class="icon"><i class="fal fa-map-marker-alt"></i></div>
							<input id="address" type="text" class="input @error('address') is-invalid @enderror" name="address" required autocomplete="address" placeholder="Street"  value="{{ old('address') }}">
							<label class="info">Adress: (Daytona Beach 45)</label>
							@error('address')
							<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
						</div>
						<div class="input_group">
							<div class="icon"><i class="fal fa-map-marker-alt"></i></div>
							<input id="address" type="text" class="input @error('zip') is-invalid @enderror" name="zip" required autocomplete="zip" placeholder="Zip"  value="{{ old('zip') }}">
							<label class="info">Zip</label>
							@error('zip')
							<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
						</div>
						<div class="input_group">
							<div class="icon"><i class="fal fa-envelope"></i></div>
							<input id="email" type="email" class="input @error('email') is-invalid @enderror" name="email" required autocomplete="email" placeholder="Email..."  value="{{ old('email') }}">
							<label class="info">Email</label>
							@error('email')
							<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
						</div>
						<div class="input_group">
							<div class="icon"><i class="fal fa-phone"></i></div>
							<input id="phone" type="phone" class="input @error('phone') is-invalid @enderror" name="phone" required autocomplete="phone" placeholder="+1 (---) --- - -- - --"  value="{{ old('phone') }}">
							<label class="info">Phone</label>
							@error('phone')
							<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
						</div>
						<div class="input_group">
							<div class="icon"><i class="fal fa-clock"></i></div>
							<input id="time_am" type="time" class="input @error('time_am') is-invalid @enderror" name="time_am" required autocomplete="time_am" placeholder=""  value="{{ old('time_am') }}">
							<label class="info">Time work AM</label>
							@error('time_am')
							<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
						</div>
						<div class="input_group">
							<div class="icon"><i class="fal fa-alarm-clock"></i></div>
							<input id="time_pm" type="time" class="input @error('time_pm') is-invalid @enderror" name="time_pm" required autocomplete="time_pm" placeholder=""  value="{{ old('time_pm') }}">
							<label class="info">Time work PM</label>
							@error('time_pm')
							<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
						</div>
						<div class="input_group">
							<div class="icon"><i class="fal fa-globe"></i></div>
							<input id="website" type="text" class="input @error('website') is-invalid @enderror" name="website" required autocomplete="website" placeholder="site.com"  value="{{ old('website') }}">
							<label class="info">Website</label>
							@error('website')
							<span class="invalid-feedback" role="alert">
									<strong>{{ $message }}</strong>
								</span>
							@enderror
						</div>
						<div class="left_image">
							<div class="you_image">
								<div class="ob_mob">
									<div class="img xx_img company--image"><img src="" alt=""></div>
								</div>
							</div>
							<div class="drop_image_f">
								<div class="wrapper_drop">
									<div class="drop">
										<div class="cont">
											<i class="fad fa-cloud-upload"></i>
											<div class="tit">
												Upload new image
											</div>
											<div class="desc">
												your files to Assets, or
											</div>
											<div class="browse">
												click here to browse
											</div>
										</div>
										<output id="list"></output><input id="files" multiple="false" name="image" type="file" />
									</div>
								</div>
							</div>
						</div>

						<div class="social_user">
							<div class="title">Add Social link</div>
							<div class="social__box">
								<div class="social__group">
									<input type="text" class="input " name="facebook" placeholder="Social link" value="">
									<div class="circle_box_sm faces"><i class="fab fa-facebook-f"></i></div>
								</div>
								<div class="social__group">
									<input type="text" class="input " name="google" placeholder="Social link" value="">
									<div class="circle_box_sm faces"><i class="fab fa-google-plus-g"></i></div>
								</div>
								<div class="social__group">
									<input type="text" class="input " name="linkedin" placeholder="Social link" value="">
									<div class="circle_box_sm faces"><i class="fab fa-linkedin-in"></i></div>
								</div>
								<div class="social__group">
									<input type="text" class="input " name="instagram" placeholder="Social link" value="">
									<div class="circle_box_sm faces"><i class="fab fa-instagram"></i></div>
								</div>
								<div class="social__group">
									<input type="text" class="input " name="twitter" placeholder="Social link" value="">
									<div class="circle_box_sm faces"><i class="fab fa-twitter"></i></div>
								</div>
								<div class="social__group">
									<input type="text" class="input " name="youtube" placeholder="Social link" value="">
									<div class="circle_box_sm faces"><i class="fab fa-youtube"></i></div>
								</div>
							</div>
						</div>
						<div class="center_btn mt20">
							<input type="submit" name="submit" class="btn_submit bg_cs" value="Add Company">
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@include('partials.footer')