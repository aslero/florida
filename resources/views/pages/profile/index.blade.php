@include('partials.head')
@include('partials.header')
@include('partials.menu')

<div class="margin_col mrt95">
	<div class="speedbar">
		<div class="container relative">
			<div class="speedbar_inner">
				<a href="{{ route('home') }}">Home</a> <i class="fas fa-angle-right"></i> User page
			</div>
		</div>
	</div>
	<div class="white_col pdt100">
		<div class="container">
			<div class="title_col_a mrb60">
				<h1 class="title">Welcome, {{Auth::user()->fullname}}</h1>
			</div>
			<div class="user_page_col">

				@include('partials.profile.sidebar')
				
				<div class="content_user">
					<form action="{{route('profile.update')}}" method="POST" class="company_box_lk">
						@csrf
						<div class="input_group mrt30">
							<div class="icon"><i class="fal fa-user"></i></div>
							<input class="input" type="text" name="fullname" value="{{Auth::user()->fullname}}" placeholder="{{Auth::user()->fullname}}">
							<label class="info">Change name</label>
						</div>
						<!--<div class="input_group">
							<div class="icon"><i class="fal fa-users"></i></div>
							<input class="input" type="text" name="text" placeholder="CCC DemiGroup">
							<label class="info">Company name</label>
						</div>-->
						<div class="input_group">
							<div class="icon"><i class="fal fa-envelope"></i></div>
							<input class="input" type="email" name="email" value="{{Auth::user()->email}}" placeholder="{{Auth::user()->email}}">
							<label class="info">Email</label>
						</div>
						<div class="input_group">
							<div class="icon"><i class="fal fa-envelope"></i></div>
							<input class="input" type="phone" name="phone" value="{{Auth::user()->phone}}" placeholder="+1 (900) 999-88-77">
							<label class="info">Phone</label>
						</div>
						<div class="input_group">
							<div class="icon"><i class="fal fa-lock-alt"></i></div>
							<input class="input form-control" type="password" name="password" value="{{Auth::user()->pass}}" placeholder="Edit Password" minlength="6">
							<div class="toggle_password"><span class="show_pass"></span></div>
						</div>
						<!--<div class="input_group">
							<div class="icon"><i class="fal fa-lock-alt"></i></div>
							<input class="input forms-control" type="password" name="password" placeholder="Forgot Password" minlength="6">
							<div class="toggle_password"><span class="show_pass"></span></div>
						</div>
						<div class="social_user">
							<div class="title_a">Add Social link</div>
							<a target="_blank" href="#" class="add_soc_user complete_link">
								<div class="circle_box_sm faces"><i class="fab fa-facebook-f"></i></div>
								<span>DemiGroup</span>
							</a>
							<a target="_blank" href="#" class="add_soc_user">
								<div class="circle_box_sm twi"><i class="fab fa-google-plus-g"></i></div>
								<span>Social Name</span>
							</a>
							<a target="_blank" href="#" class="add_soc_user">
								<div class="circle_box_sm gps"><i class="fab fa-linkedin-in"></i></div>
								<span>Social Name</span>
							</a>
						</div>-->
						<input type="submit" name="submit" class="btn_submit bg_cs" value="Save setting">
					</form>
					<div class="company_box_lk pd_35">
						<div class="title">Billing</div>
						<div class="tarification">
							<div class="at">{{$tariff->title}}</div>
							@if($tariff->price == 0)<a class="ed" href="{{route('price')}}">Upgrade to Premium <i class="fas fa-edit"></i></a>@endif
						</div>
						<div class="info_pay">Service is active: <a href="#">{{\Illuminate\Support\Carbon::parse($latestPayment->date_to)->format('d.m.Y')}}</a></div>
						<div class="title">Payments</div>
						@forelse($payments as $payment)
							<div class="pay_txt"><b>{{\Illuminate\Support\Carbon::parse($payment->created_at)->format('d.m.Y')}}</b> <span>${{$payment->cost}}</span></div>
							@empty
						@endforelse
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@if($tariff->price > 0 && $latestPayment->status == 0)
	<div class="drop_info_up modal_open" id="gift">
		<div class="data_drop_col">
			<div class="title">Buy on Tarif Plan</div>
			<div class="content_user">
				<div class="company_box_lk wd100">
					<div class="tarification_checkout">
						<div class="info">
							<span>Client:</span>
							<div class="summ">{{Auth::user()->fullname}}</div>
						</div>
						<div class="info" data-id="2">
							<span>Tarification:</span>
							<div class="btn_p">{{$tariff->title}}</div>
						</div>
						<!--<div class="info">
                          <span>Tarification:</span>
                          <div class="btn_f">Free</div>
                        </div>-->
						<div class="info hidden" data-id="1">
							<span>Tarification:</span>
							<div class="btn_ps">{{$tariff->title}}</div>
						</div>
						<div class="info hidden">
							<span>Payment:</span>
							<div class="summ">$<span class="check--sum">{{$tariff->price}}</span>/year</div>
						</div>
					</div>
					<div class="row">
						<div class="clr"></div>
						<div class="wd50_inp">
							<div class="input_group">
								<div class="icon"><i class="fal fa-tags"></i></div>
								<input class="input" type="text" id="promocode" name="text" placeholder="Promo code...">
								<label class="info">promo code</label>
							</div>
						</div>
						<div class="wd50_inp">
							<input type="button" name="submit" id="btn_promo_aply" class="btn_submit bg_cs_apply" value="Apply Code">
						</div>
					</div>
					<div class="total_pay">Total Pay: $<span class="check--sum">{{$tariff->price}}</span></div>
					<div class="center_btn mt20">
						<form action="{{route('payments')}}" method="GET" id="check--from">
							<input type="hidden" name="promocode" value="0">
							<input type="hidden" name="price" value="{{$tariff->price}}">
							<input type="hidden" name="id" value="{{$tariff->id}}">
							<input type="submit" name="submit" class="btn_submit bg_cs" value="Checkout">
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal_overlay" style="display: block;"></div>
@endif

@include('partials.footer')