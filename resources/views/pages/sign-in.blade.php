@include('partials.head')

<body>
<div class="home_col left_sign_col">
	<div class="home_box left_sign wow fadeIn" data-wow-duration="0.3s" data-wow-delay="0.1s">
		<div class="logo"><img src="{{ asset('assets/img/logo.png') }}" alt=""></div>
		<h1 class="title">Your digital marketing assistant that never <br> sleeps</h1>
		<p class="descr">An easier way to manage and automate your marketing.
		</p>
		<div class="btn">LEARN MORE <i class="fas fa-angle-right"></i></div>
	</div>
	<div class="right_sign">
		<div class="no_register wow fadeIn" data-wow-duration="0.3s" data-wow-delay="0.4s"><span>Don't have an account?</span> <a href="#">Create account</a></div>
		<div class="window_sign wow fadeIn" data-wow-duration="0.3s" data-wow-delay="0.2s">
			<div class="title">Sign in</div>
			<div class="descr">Enter your details below to sign in.</div>
			<div class="input_group mrt30">
				<div class="icon"><i class="fal fa-envelope"></i></div>
				<input class="input" type="text" name="email" placeholder="Your login or email here..">
			</div>
			<div class="input_group">
				<div class="icon"><i class="fal fa-lock-alt"></i></div>
				<input class="input form-control" type="password" name="password" placeholder="Password" minlength="6">
				<div class="toggle_password"><span class="show_pass"></span></div>
			</div>
			<div class="cheker">
				<label>
				<input type="checkbox" checked="">
				<span></span>
				<small class="rmb">Remember me</small>
				</label>
				<a href="#" class="forget_password">Forget Password?</a>
			</div>
			<input type="submit" name="submit" class="btn_submit" value="Sign in">
			<div class="social_login">
				<div class="descr">	or login with:</div>
				<a class="circle_box faces" target="_blank" href="#"><i class="fab fa-facebook-f"></i></a>
				<a class="circle_box twi" target="_blank" href="#"><i class="fab fa-google-plus-g"></i></a>
				<a class="circle_box gps" target="_blank" href="#"><i class="fab fa-linkedin-in"></i></a>
			</div>
		</div>
	</div>
	<div id="particles-js">
		<canvas class="particles-js-canvas-el" width="1349" height="500" style="width: 100%; height: 100%;"></canvas>
	</div>
</div>
<div class="line_z"></div>
<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('assets/js/particles.js') }}"></script>
<link href="{{ asset('assets/css/animate.css') }}" rel="stylesheet">
<link href="{{ asset('assets/css/all.min.css') }}" rel="stylesheet">
<script src="{{ asset('assets/js/slick.min.js') }}"></script>
<script src="{{ asset('assets/js/main.js') }}"></script>
<script src="{{ asset('assets/js/jquery.fancybox.min.js') }}"></script>
<link href="{{ asset('assets/css/jquery.fancybox.min.css') }}" rel="stylesheet">
<script src="{{ asset('assets/js/wow.min.js') }}"></script>
<script>
	new WOW().init();
</script>
</body>
</html>