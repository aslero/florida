@include('partials.head')
@include('partials.header')
@include('partials.menu')

<div class="margin_col">
	<div class="home_col about_us">
		<div class="container relative">
			<div class="img_about wow fadeIn" data-wow-duration="0.3s" data-wow-delay="0.1s"><img src="{{$content->img}}" alt="{{$content->pagetitle}}"></div>
			<div class="home_box wow fadeIn" data-wow-duration="0.3s" data-wow-delay="0.2s">
				<h1 class="title">{{$content->pagetitle}}</h1>
				<p class="descr">{{$content->description}}
				</p>
				<a href="{{route('add-company')}}" class="btn">BUILD MY AD CAMPAIGN <i class="fas fa-angle-right"></i></a>
			</div>
		</div>
		<div id="particles-js">
			<canvas class="particles-js-canvas-el" width="1349" height="500" style="width: 100%; height: 100%;"></canvas>
		</div>
	</div>
	<div class="white_col pdt100">
		<div class="container">
			<div class="cont_news wow fadeIn">
				{!! $content->content !!}
			</div>
		</div>
	</div>
	<div class="gray_col pdt100">
		<div class="container">
			<div class="txt_contacts wow fadeIn" data-wow-duration="0.3s" data-wow-delay="0.1s">
				<div class="title">{{$contact->pagetitle}}</div>
				<div class="descr">
					{!! $contact->content !!}
				</div>
				<div class="phone_head no_mob_contact">
					<div class="circles_cols phs"> <i style="color: #33f1ac;" class="far fa-at"></i> </div>
					{{$contacts->email}}
					<div class="schedule">
						<div>{{$contacts->email_work}}</div>
					</div>
				</div>
				<div class="phone_head no_mob_contact">
					<div class="circles_cols"><i style="color: #0e8fef;"  class="fab fa-skype"></i></div>
					{{$contacts->skype}}
					<div class="schedule">
						<div>{{$contacts->skype_work}}</div>
					</div>
				</div>
			</div>
			<div class="contacts_post wow fadeIn" data-wow-duration="0.3s" data-wow-delay="0.2s">
				<div class="input_group">
					<div class="icon"><i class="fal fa-user"></i></div>
					<input class="input" type="text" name="name" placeholder="Your name">
				</div>
				<div class="input_group">
					<div class="icon"><i class="fal fa-envelope"></i></div>
					<input class="input" type="text" name="email" placeholder="Work email">
				</div>
				<div class="input_group">
					<div class="icon"><i class="fal fa-comment-lines"></i></div>
					<textarea class="input" placeholder="Message"></textarea>
				</div>
				<div class="cheker">
					<label>
					<input type="checkbox" checked="">
					<span></span>
					<small class="rmb">I agree to the <a href="#">Terms of Service & Privacy Policy</a></small>
					</label>
					<br><br>
					<div class="txt">Too long? We don't share your data and we won't send you marketing emails. We will however send you product messages to help you get the most from Adsinflorida.com</div>
				</div>
				<input type="submit" name="submit" class="btn_submit" value="Send Message">
			</div>
		</div>
	</div>
</div>

@include('partials.footer')