@extends('layouts.admin')
@section('h1','Statistic - '.$service->label)
@section('content')
    <services-stats id="{{$service->id}}"></services-stats>
@endsection
