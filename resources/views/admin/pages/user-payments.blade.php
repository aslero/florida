@extends('layouts.admin')
@section('h1','User payments')
@section('content')
    <user-payments :id="{{$id}}"></user-payments>
@endsection
