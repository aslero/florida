@extends('layouts.admin')
@section('h1','Report images '.$report->label)
@section('content')
    <reports-galleries id="{{$report->id}}"></reports-galleries>
@endsection
