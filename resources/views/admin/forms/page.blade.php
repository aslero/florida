@extends('layouts.admin')
@section('h1','Edit page')
@section('content')
    <pages-edit :id="{{$id}}"></pages-edit>
@endsection
