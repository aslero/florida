<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Авторизация</title>
    <script src="{{ asset('js/admin.js') }}"defer></script>
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
</head>
<body class="auth">
    <div id="app">
        <login></login>
    </div>
</body>
</html>

