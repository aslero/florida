@include('partials.head')

<body>
<div class="home_col left_sign_col">
    <div class="home_box left_sign wow fadeIn" data-wow-duration="0.3s" data-wow-delay="0.1s">
        <a href="{{route('home')}}" class="logo"><img src="{{ asset('assets/img/logo.png') }}" alt=""></a>
        <h1 class="title">Your digital marketing assistant that never <br> sleeps</h1>
        <p class="descr">An easier way to manage and automate your marketing.
        </p>
        <a href="{{ route('home') }}" class="btn">HOME <i class="fas fa-angle-right"></i></a>
    </div>
    <div class="right_sign over_sign">
        <div class="no_register wow fadeIn" data-wow-duration="0.3s" data-wow-delay="0.4s"><span>Already have an account?</span> <a href="{{route('login')}}">Sign in</a></div>
        <form  method="POST" action="{{ route('register') }}" class="window_sign wow fadeIn" data-wow-duration="0.3s" data-wow-delay="0.2s">
            @csrf
            <input type="hidden" name="tariff" value="{{$tariff}}">
            <div class="title">Start Sign up</div>
            <div class="descr">No credit card required.</div>
            <div class="input_group mrt30">
                <div class="icon"><i class="fal fa-business-time"></i></div>
                <input id="name" type="text" class="input @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Account name">

                @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <!--<div class="input_group">
                <div class="icon"><i class="fal fa-users"></i></div>
                <input id="company" type="text" class="input forms-control @error('company') is-invalid @enderror" name="company" required autocomplete="company" placeholder="Company name">
                @error('company')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>-->
            <div class="input_group">
                <div class="icon"><i class="fal fa-user"></i></div>
                <input id="fullname" type="text" class="input form-control @error('fullname') is-invalid @enderror" name="fullname" required autocomplete="fullname" placeholder="Full name">
                @error('fullname')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="input_group">
                <div class="icon"><i class="fal fa-envelope"></i></div>
                <input id="email" type="email" class="input @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Work email">

                @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="input_group">
                <div class="icon"><i class="fal fa-lock-alt"></i></div>
                <input id="password" type="password" class="input form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Password" minlength="6">

                @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
                <div class="toggle_password"><span class="show_pass"></span></div>
            </div>
            <div class="cheker">
                <label>
                    <input  type="checkbox" name="police" id="police" {{ old('remember') ? 'checked' : '' }} required>
                    <span></span>
                    <small class="rmb">I agree to the <a href="#">Terms of Service & Privacy Policy</a></small>
                </label>
                <br><br>
                <div class="txt">Too long? We don't share your data and we won't send you marketing emails. We will however send you product messages to help you get the most from Adsinflorida.com</div>
            </div>
            <div class="recaptcha_group">
                {!! Captcha:: display()!!}
                @error('g-recaptcha-response')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <input type="submit" name="submit" class="btn_submit" value="Get started">
            <div class="social_login">
                <div class="descr">	or login with:</div>
                <a class="circle_box faces" href="/login/facebook"><i class="fab fa-facebook-f"></i></a>
                <a class="circle_box twi" href="/login/google"><i class="fab fa-google-plus-g"></i></a>
                <a class="circle_box gps" href="/login/linkedin"><i class="fab fa-linkedin-in"></i></a>
            </div>
        </form>
    </div>
    <div id="particles-js">
        <canvas class="particles-js-canvas-el" width="1349" height="500" style="width: 100%; height: 100%;"></canvas>
    </div>
</div>
<div class="line_z"></div>
<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('assets/js/particles.js') }}"></script>
<link href="{{ asset('assets/css/animate.css') }}" rel="stylesheet">
<link href="{{ asset('assets/css/all.min.css') }}" rel="stylesheet">
<script src="{{ asset('assets/js/slick.min.js') }}"></script>
<script src="{{ asset('assets/js/main.js') }}"></script>
<script src="{{ asset('assets/js/jquery.fancybox.min.js') }}"></script>
<link href="{{ asset('assets/css/jquery.fancybox.min.css') }}" rel="stylesheet">
<script src="{{ asset('assets/js/wow.min.js') }}"></script>
<script src="{{ asset('js/app.js') }}" defer></script>
<script>
    new WOW().init();
</script>
</body>
</html>
