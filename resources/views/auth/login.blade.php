@include('partials.head')

<body>
<div class="home_col left_sign_col">
    <div class="home_box left_sign wow fadeIn" data-wow-duration="0.3s" data-wow-delay="0.1s">
        <a href="{{route('home')}}" class="logo"><img src="{{ asset('assets/img/logo.png') }}" alt=""></a>
        <h1 class="title">Your digital marketing assistant that never <br> sleeps</h1>
        <p class="descr">An easier way to manage and automate your marketing.
        </p>
        <a href="{{ route('home') }}" class="btn">HOME <i class="fas fa-angle-right"></i></a>
    </div>
    <div class="right_sign">
        <div class="no_register wow fadeIn" data-wow-duration="0.3s" data-wow-delay="0.4s"><span>Don't have an account?</span> <a href="{{route('register')}}">Create account</a></div>
        <form  method="POST" action="{{ route('login') }}" class="window_sign wow fadeIn" data-wow-duration="0.3s" data-wow-delay="0.2s">
            @csrf
            <div class="title">Sign in</div>
            <div class="descr">Enter your details below to sign in.</div>
            <div class="input_group mrt30">
                <div class="icon"><i class="fal fa-envelope"></i></div>
                <input id="email" type="text" class="input {{ $errors->has('name') || $errors->has('email') ? ' is-invalid' : '' }}" name="login" value="{{ old('name') ?: old('email') }}" required autocomplete="email" autofocus placeholder="Your email here..">
                @if ($errors->has('name') || $errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('name') ?: $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="input_group">
                <div class="icon"><i class="fal fa-lock-alt"></i></div>
                <input id="password" type="password" class="input form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Password" minlength="6">
                @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
                <div class="toggle_password"><span class="show_pass"></span></div>
            </div>
            <div class="cheker">
                <label>
                    <input  type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                    <span></span>
                    <small class="rmb">Remember me</small>
                </label>
                <a href="{{ route('password.request') }}" class="forget_password">Forget Password?</a>
            </div>
            <div class="recaptcha_group">
                {!! Captcha:: display()!!}
                @error('g-recaptcha-response')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <input type="submit" name="submit" class="btn_submit" value="Sign in">
            <div class="social_login">
                <div class="descr">	or login with:</div>
                <a class="circle_box faces" href="/login/facebook"><i class="fab fa-facebook-f"></i></a>
                <a class="circle_box twi" href="/login/google"><i class="fab fa-google-plus-g"></i></a>
                <a class="circle_box gps" href="/login/linkedin"><i class="fab fa-linkedin-in"></i></a>
            </div>
        </form>
    </div>
    <div id="particles-js">
        <canvas class="particles-js-canvas-el" width="1349" height="500" style="width: 100%; height: 100%;"></canvas>
    </div>
</div>
<div class="line_z"></div>
<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('assets/js/particles.js') }}"></script>
<link href="{{ asset('assets/css/animate.css') }}" rel="stylesheet">
<link href="{{ asset('assets/css/all.min.css') }}" rel="stylesheet">
<script src="{{ asset('assets/js/slick.min.js') }}"></script>
<script src="{{ asset('assets/js/main.js') }}"></script>
<script src="{{ asset('assets/js/jquery.fancybox.min.js') }}"></script>
<link href="{{ asset('assets/css/jquery.fancybox.min.css') }}" rel="stylesheet">
<script src="{{ asset('assets/js/wow.min.js') }}"></script>
<script src="{{ asset('js/app.js') }}" defer></script>
<script>
    new WOW().init();
</script>
</body>
</html>