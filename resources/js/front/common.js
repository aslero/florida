
$(function(){
    const token = $('[name="csrf-token"]').attr('content');

    $('body').on('click', '.delete-company', function () {
        let id = $(this).attr('data-id');
        toastDelete.fire({
            title: 'Delete a company?',
            type: 'warning',
            showCancelButton: true,
            cancelButtonColor: '#1b053e',
            confirmButtonColor: '#ff194e',
            cancelButtonText: 'Close',
            confirmButtonText: 'Delete'

        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: "/profile/business/delete",
                    data: { // что отправляем
                        "id": id,
                        '_token': token
                    },
                    cache: false,
                    success: function (response) {
                        if (response.error === 0){
                            toast.fire({
                                icon: 'success',
                                title: response.message
                            });
                        }
                        if (response.error === 1){
                            toast.fire({
                                icon: 'error',
                                title: response.message
                            });
                        }
                        if (response.error === 2){
                            window.location.href = response.link;
                        }
                        $('body').find('.comp_ave_box[data-id='+id+']').remove();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        toast.fire({
                            icon: 'error',
                            title: jqXHR.responseJSON.message
                        });
                        // TODO jqXHR.responseJSON может не быть
                    }
                });
            }
        });

    });

    $("#reviews").submit(function(e) {
        e.preventDefault();
        let form_data = $(this).serialize();
        $.ajax({
            type: "POST",
            url: $(this).attr('action'),
            data: form_data,
            cache: false,
            success: function (response) {
                if (response.error === 0){
                    toast.fire({
                        icon: 'success',
                        title: response.message
                    });
                    $("forms#reviews").trigger('reset');
                }
                if (response.error === 1){
                    toast.fire({
                        icon: 'error',
                        title: response.message
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toast.fire({
                    icon: 'error',
                    title: jqXHR.responseJSON.message
                });
                // TODO jqXHR.responseJSON может не быть
            }
        });
    });


    $("#btn_promo_aply").on('click',function(e) {
        e.preventDefault();
        let promo = $('#promocode').val();
        let tariff = $('#check--from input[name=id]').val();
        if (promo.length == 0 || !promo){
            toast.fire({
                icon: 'error',
                title: 'Enter your promotional code'
            });
        }
        $.ajax({
            type: "GET",
            url: '/promocode',
            data: {
                promo: promo,
                tariff: tariff,
            },
            cache: false,
            success: function (response) {
                if (response.error === 0){
                    $('#check--from input[name=promocode]').val(response.promocode);
                    $('#check--from input[name=price]').val(response.sale);
                    $('.check--sum').text(response.sale);
                }
                if (response.error === 1){
                    toast.fire({
                        icon: 'error',
                        title: response.message
                    });
                }
                if (response.error === 2){
                    window.location.href = response.link;
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toast.fire({
                    icon: 'error',
                    title: jqXHR.responseJSON.message
                });
                // TODO jqXHR.responseJSON может не быть
            }
        });
    });


    $("#contacts").submit(function(e) {
        e.preventDefault();
        let form_data = $(this).serialize();
        $.ajax({
            type: "POST",
            url: $(this).attr('action'),
            data: form_data,
            cache: false,
            success: function (response) {
                if (response.error === 0){
                    toast.fire({
                        icon: 'success',
                        title: response.message
                    });
                    $("forms#contacts").trigger('reset');
                }
                if (response.error === 1){
                    toast.fire({
                        icon: 'error',
                        title: response.message
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                toast.fire({
                    icon: 'error',
                    title: jqXHR.responseJSON.message
                });
                // TODO jqXHR.responseJSON может не быть
            }
        });
    });

    $( document ).ready(function() {

        $('input, textarea').on('focus focusout', function(event) {

            $e = $(this);

            if (event.type == 'focus') {
                $placeholder = $e.attr("placeholder");
                $e.attr("ph-input", $placeholder);
                $placeholder = '';
            }
            if (event.type == 'focusout') {
                $placeholder = $e.attr("ph-input");
            }
            $e.attr("placeholder", $placeholder);
        });

    });

    $('input').on('change invalid', function() {
        var textfield = $(this).get(0);

        // 'setCustomValidity not only sets the message, but also marks
        // the field as invalid. In order to see whether the field really is
        // invalid, we have to remove the message first
        textfield.setCustomValidity('');

        if (!textfield.validity.valid) {
            textfield.setCustomValidity('Fill in this field!');
        }
    });
});