/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import swal from 'sweetalert2';
window.swal = swal;

const toast = swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    onOpen: (toast) => {
        toast.addEventListener('mouseenter', swal.stopTimer)
        toast.addEventListener('mouseleave', swal.resumeTimer)
    }
});

window.toast = toast;

const toastDelete = swal.mixin({
    showCancelButton: true
});

window.toastDelete = toastDelete;

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))
Vue.component('login', require('./components/Admin/Auth/Login.vue').default);

Vue.component('slider', require('./components/Admin/Slider/Slider.vue').default);

Vue.component('contacts', require('./components/Admin/Contacts/Contacts.vue').default);

Vue.component('reviews', require('./components/Admin/Reviews/Reviews.vue').default);

Vue.component('promocodes', require('./components/Admin/Promocodes/Promocodes.vue').default);

Vue.component('domains', require('./components/Admin/Domains/Domains.vue').default);

Vue.component('users', require('./components/Admin/Users/Users.vue').default);
Vue.component('user-payments', require('./components/Admin/Users/UserPayments.vue').default);

Vue.component('faqs', require('./components/Admin/Faqs/Faqs.vue').default);

Vue.component('companies', require('./components/Admin/Company/Companies.vue').default);

Vue.component('payments', require('./components/Admin/Payments/Payments.vue').default);

Vue.component('services', require('./components/Admin/Services/Services.vue').default);
Vue.component('services-stats', require('./components/Admin/Services/Stats.vue').default);

Vue.component('reports', require('./components/Admin/Reports/Reports.vue').default);
Vue.component('reports-galleries', require('./components/Admin/Reports/Galleries.vue').default);

Vue.component('pages', require('./components/Admin/Pages/Pages.vue').default);
Vue.component('pages-edit', require('./components/Admin/Pages/Edit.vue').default);

Vue.component('countries', require('./components/Admin/Country/Country.vue').default);
Vue.component('cities', require('./components/Admin/Cities/Cities.vue').default);

Vue.component('categories', require('./components/Admin/Categories/Categories.vue').default);

Vue.component('compares', require('./components/Admin/Compares/Compares.vue').default);

Vue.component('tariffs', require('./components/Admin/Tariffs/Tariffs.vue').default);
Vue.component('tariff-edit', require('./components/Admin/Tariffs/Edit.vue').default);
Vue.component('tariff-create', require('./components/Admin/Tariffs/Create.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});
